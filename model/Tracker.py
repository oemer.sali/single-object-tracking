import torch
import model.DarkNet
import model.RPNHead
import model.RPN
import model.KalmanFilter
import model.Memory
import model.LSTMCell


class Tracker(torch.nn.Module):
    def __init__(self, config):
        super(Tracker, self).__init__()
        self.config = config

        self.darknet = model.DarkNet.Network(config=config)
        self.rpn_head = model.RPNHead.MultiRPNHead(config=config)
        self.rpn = model.RPN.RPN(config=config)

        # D = torch.eye(n=config.Tracker.hidden_size).cuda()
        # D[0, 4] = D[1, 5] = 0.0
        # M = torch.eye(n=config.Tracker.observation_size, m=config.Tracker.hidden_size).cuda()
        # Q = torch.eye(n=config.Tracker.hidden_size, m=config.Tracker.hidden_size).cuda()
        # Q[0:2] *= 0.16
        # Q[2:4] *= 0.04
        # R = torch.eye(n=config.Tracker.observation_size, m=config.Tracker.observation_size).cuda()
        # R[0:2] *= 0.08
        # R[2:4] *= 0.08
        #
        # self.kalman_filter = model.KalmanFilter.KalmanFilter(D=D, M=M, Q=Q, R=R)

    def initialize(self, frame, box):
        """Initializes the tracker with the bounding box on the first frame.

        :param frame: Initial frame of the video sequence.
        :type frame: tensor<float>[batch_size, channels, height, width]

        :param box: Initial bounding box of the tracked object.
        :type box: tensor<float>[batch_size, (y, x, h, w)]
        """
        self.counter = 0

        # x_p = torch.as_tensor([box[0][0], box[0][1], box[0][2], box[0][3], 0.0, 0.0]).cuda()
        # Sigma_p = torch.zeros(self.config.Tracker.hidden_size, self.config.Tracker.hidden_size).cuda()
        # self.kalman_filter.initialize(x_p=x_p, Sigma_p=Sigma_p)
        patch = self._calc_patch(
            image_box=box,
            scale=1.0
        )
        exemplar_patch = self._grid_sample(
            frame=frame,
            box=patch,
            shape=self.config.Tracker.exemplar_shape
        ).detach()

        self.exemplar_embedding = self.darknet(exemplar_patch)
        self.box = box

    def forward(self, frame):
        """Perform the inference for the next frame of the video sequence.

        :param frame: Next frame of the input video.
        :type frame: tensor<float>[batch_size, channels, height, width]

        :return:
        """
        # Crop patch with the double size of last position
        image_box = self.box
        # image_box = torch.unsqueeze(self.kalman_filter.predict(), 0)
        patch = self._calc_patch(
            image_box=image_box,
            scale=2.0
        )
        patch_box = self._image_box_to_patch_box(
            patch=patch,
            shape=self.config.Tracker.search_shape,
            image_box=image_box
        )
        search_patch = self._grid_sample(
            frame=frame,
            box=patch,
            shape=self.config.Tracker.search_shape
        ).detach()

        search_embedding = self.darknet(search_patch)
        score_logits, box_deltas = self.rpn_head(search=search_embedding, exemplar=self.exemplar_embedding)
        scores, patch_box, anchor = self.rpn(
            box_updates=patch_box,
            score_logits=score_logits,
            box_deltas=box_deltas
        )

        image_box = self._patch_box_to_image_box(patch=patch, shape=self.config.Tracker.search_shape, patch_box=patch_box)
        delta_center, delta_size = torch.split(tensor=image_box, split_size_or_sections=2, dim=1)
        box_center, box_size = torch.split(tensor=self.box, split_size_or_sections=2, dim=1)

        # center = torch.lerp(input=box_center, end=delta_center, weight=self.config.Tracker.center_momentum)
        center = delta_center

        _, fg_score = torch.split(scores, split_size_or_sections=1, dim=1)
        size_momentum = self.config.Tracker.size_momentum * fg_score
        size = torch.lerp(input=box_size, end=delta_size, weight=size_momentum)

        box = torch.cat(tensors=(center, size), dim=1).detach()
        # box = torch.unsqueeze(self.kalman_filter.update(torch.squeeze(box, 0)), 0).detach()

        self.box = box

        box = torch.clamp(box, 0.0, 10000)
        return box

    # def _softargmax(self, response_map):
    #     """
    #
    #     :param response_map: [batch_size, height, width]
    #     :return:
    #     """
    #
    #     batch_size, height, width = response_map.shape
    #     response_map = response_map.view(batch_size, height * width, 1)
    #     response_map = response_map - torch.max(input=response_map, dim=1, keepdim=True)[0]
    #     response_map = torch.nn.functional.softmax(input=response_map, dim=1)
    #
    #     # response_map_ = response_map.view(batch_size, height, width)
    #     # print(response_map_)
    #
    #     grid_y = torch.arange(start=0.0, end=height, step=1.0, dtype=response_map.dtype, device=response_map.device)
    #     grid_x = torch.arange(start=0.0, end=width, step=1.0, dtype=response_map.dtype, device=response_map.device)
    #     grid = torch.stack(torch.meshgrid([grid_y, grid_x]), 2).view(1, height * width, 2)
    #
    #     position = response_map * grid
    #     position = torch.sum(input=position, dim=1, keepdim=False)
    #
    #     return position

    @staticmethod
    def _calc_patch(image_box, scale):
        """Calculates the patch to crop given the object's image coordinates.

        :param image_box: Bounding box in image space.
        :type image_box: tensor<float>[batch_size, (y, x, h, w)]

        :param scale: Scaling factor for the patch area.
        :type scale: float

        :return patch: Anchor box of image patch.
         :rtype patch: tensor<float>[batch_size, (y, x, h, w)]
        """
        image_box_center, image_box_size = torch.split(tensor=image_box, split_size_or_sections=2, dim=1)
        image_box_height, image_box_width = torch.split(tensor=image_box_size, split_size_or_sections=1, dim=1)
        padding = (image_box_height + image_box_width) / 2.0
        length = scale * torch.sqrt((image_box_height + padding) * (image_box_width + padding))
        patch = torch.cat(tensors=(image_box_center, length, length), dim=1)
        return patch

    @staticmethod
    def _image_box_to_patch_box(patch, shape, image_box):
        """Warps the box given in image coordinates to a box in patch coordinates.

        :param patch: Anchor box of image patch.
        :type patch: tensor<float>[batch_size, (y, x, h, w)]

        :param shape: Shape of warped image patch.
        :type shape: tuple<int>[(height, width)]

        :param image_box: Bounding box in image space.
        :type image_box: tensor<float>[batch_size, (y, x, h, w)]

        :return patch_box: Bounding box in patch space.
        :rtype patch_box: tensor<float>[batch_size, (y, x, h, w)]
        """
        patch_center, patch_size = torch.split(tensor=patch, split_size_or_sections=2, dim=1)
        shape = torch.as_tensor(data=shape, dtype=torch.float, device=patch.device)
        image_box_center, image_box_size = torch.split(tensor=image_box, split_size_or_sections=2, dim=1)

        center_diff = image_box_center - patch_center
        center_diff /= patch_size - 1.0  # Normalized to -1.0..+1.0
        center_diff *= shape - 1.0
        patch_box_center = (shape - 1.0) / 2.0 + center_diff

        size_ratio = image_box_size / patch_size
        patch_box_size = shape * size_ratio

        patch_box = torch.cat(tensors=[patch_box_center, patch_box_size], dim=1)
        return patch_box

    @staticmethod
    def _patch_box_to_image_box(patch, shape, patch_box):
        """Warps the box given in image coordinates to a box in patch coordinates.

        :param patch: Anchor box of image patch.
        :type patch: tensor<float>[batch_size, (y, x, h, w)]

        :param shape: Shape of warped image patch.
        :type shape: tuple<int>[(height, width)]

        :param patch_box: Bounding box in patch space.
        :type patch_box: tensor<float>[batch_size, (y, x, h, w)]

        :return image_box: Bounding box in image space.
        :rtype image_box: tensor<float>[batch_size, (y, x, h, w)]
        """
        patch_center, patch_size = torch.split(tensor=patch, split_size_or_sections=2, dim=1)
        shape = torch.as_tensor(data=shape, dtype=torch.float, device=patch.device)
        patch_box_center, patch_box_size = torch.split(tensor=patch_box, split_size_or_sections=2, dim=1)

        center_diff = patch_box_center - (shape - 1.0) / 2.0
        center_diff /= shape - 1.0
        center_diff *= patch_size - 1.0  # Normalized to -1.0..+1.0
        image_box_center = patch_center + center_diff

        size_ratio = patch_box_size / shape
        image_box_size = patch_size * size_ratio

        image_box = torch.cat(tensors=[image_box_center, image_box_size], dim=1)
        return image_box

    @staticmethod
    def _grid_sample(frame, box, shape):
        """Crops and warps patches from frame according to bounding box with bilinear interpolation.

        :param frame: Batch of input frames to crop.
        :type frame: tensor<float>[batch_size, input_channels, input_height, input_width]

        :param box: Batch of bounding boxes to crop at.
        :type box: tensor<float>[batch_size, (y, x, h, w)]

        :param shape: Common shape of cropped patches.
        :type shape: tuple<int>[(output_height, output_width)]

        :return crop: Cropped and warped patches.
        :rtype crop: tensor<float>[batch, input_channels, output_height, output_width]
        """
        batch_size, input_channels, input_height, input_width = frame.shape
        output_height, output_width = shape

        y, x, h, w = torch.split(tensor=box, split_size_or_sections=1, dim=1)
        zeros = torch.zeros(batch_size, 1, dtype=box.dtype, device=box.device)
        theta = torch.cat(tensors=(
            (w - 1.0) / (input_width - 1.0), zeros, 2.0 * x / (input_width - 1.0) - 1.0,
            zeros, (h - 1.0) / (input_height - 1.0), 2.0 * y / (input_height - 1.0) - 1.0
        ), dim=1).view(batch_size, 2, 3)
        grid = torch.nn.functional.affine_grid(
            theta=theta,
            size=torch.Size((batch_size, input_channels, output_height, output_width))
        )
        crop = torch.nn.functional.grid_sample(
            input=frame,
            grid=grid,
            mode='bilinear',
            padding_mode='zeros'
        )
        return crop
