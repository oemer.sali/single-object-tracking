import torch


class KalmanFilter(object):
    def __init__(self, D, M, Q, R):
        """Initialize the Kalman Filter model.

        :param D: Linear dynamics model process matrix.
        :type D: tensor<float>[hidden_size, hidden_size]

        :param M: Linear measurement model process matrix
        :type M: tensor<float>[observation_size, hidden_size]

        :param Q: Linear dynamics model process covariance.
        :type Q: tensor<float>[hidden_size, hidden_size]

        :param R: Linear dynamics model process covariance.
        :type R: tensor<float>[observation_size, observation_size]
        """
        self.D, self.M = D, M
        self.Q, self.R = Q, R
        self.hidden_size, self.observation_size = D.shape[0], M.shape[0]

    def initialize(self, x_p, Sigma_p):
        """Initialize the hidden state.

        :param x_p: A posteriori estimate of hidden state.
        :type x_p: tensor<float>[hidden_size]

        :param Sigma_p: A posteriori error covariance.
        :type Sigma_p: tensor<float>[hidden_size, hidden_size]
        """
        self.x_p = x_p
        self.Sigma_p = Sigma_p

    def predict(self):
        """Predict a priori hidden state and error covariance.

        :return x_m: A priori estimate of hidden state.
        :rtype x_m: tensor<float>[hidden_size]

        :return Sigma_m: A priori error covariance.
        :rtype Sigma_m: tensor<float>[hidden_size, hidden_size]
        """
        self.x_m = torch.mv(self.D, self.x_p)
        self.Sigma_m = torch.mm(input=self.D, mat2=self.Sigma_p)
        self.Sigma_m = torch.addmm(mat1=self.Sigma_m, mat2=torch.t(input=self.D), input=self.Q)
        y_m = torch.mv(self.M, self.x_m)
        return y_m

    def update(self, y):
        """Update hidden state and error covariance with measurement.

        :param y: Measurement from the sensor.
        :type y: tensor<float>[observation_size]

        :return x_p: A posteriori estimate of hidden state.
        :rtype x_m: tensor<float>[hidden_size]

        :return Sigma_m: A posteriori error covariance.
        :rtype Sigma_m: tensor<float>[hidden_size, hidden_size]
        """
        K = torch.mm(input=self.M, mat2=self.Sigma_m)
        K = torch.addmm(mat1=K, mat2=torch.t(self.M), input=self.R)
        K = torch.mm(input=torch.t(self.M), mat2=torch.inverse(input=K))
        K = torch.mm(input=self.Sigma_m, mat2=K)
        r = torch.addmv(mat=self.M, vec=-self.x_m, input=y)
        R = torch.addmm(mat1=K, mat2=-self.M, input=torch.eye(n=self.hidden_size).cuda())
        self.x_p = torch.addmv(mat=K, vec=r, input=self.x_m)
        self.Sigma_p = torch.mm(input=R, mat2=self.Sigma_m)
        y_p = torch.mv(self.M, self.x_p)
        return y_p
