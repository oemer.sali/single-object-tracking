import torch


class LSTMCell(torch.nn.Module):
    def __init__(self, state_features, input_features, bias=True, activation_fn=torch.nn.Tanh()):
        super(LSTMCell, self).__init__()
        self.forget_gate = torch.nn.Sequential(
            torch.nn.Linear(
                in_features=input_features+state_features,
                out_features=state_features,
                bias=bias
            ),
            torch.nn.Sigmoid()
        )
        self.input_gate = torch.nn.Sequential(
            torch.nn.Linear(
                in_features=input_features + state_features,
                out_features=state_features,
                bias=bias
            ),
            torch.nn.Sigmoid()
        )
        self.output_gate = torch.nn.Sequential(
            torch.nn.Linear(
                in_features=input_features + state_features,
                out_features=state_features,
                bias=bias
            ),
            torch.nn.Sigmoid()
        )
        self.cell_state = torch.nn.Sequential(
            torch.nn.Linear(
                in_features=input_features + state_features,
                out_features=state_features,
                bias=bias
            ),
            activation_fn
        )
        self.hidden_state = activation_fn

    def forward(self, state, input):
        cell_state, hidden_state = state
        controller = torch.cat(tensors=[hidden_state, input], dim=1)
        forget_gate = self.forget_gate(controller)
        input_gate = self.input_gate(controller)
        output_gate = self.output_gate(controller)
        cell_state_candidate = self.cell_state(controller)
        cell_state = forget_gate*cell_state+input_gate*cell_state_candidate
        hidden_state_candidate = self.hidden_state(cell_state)
        hidden_state = output_gate*hidden_state_candidate
        return cell_state, hidden_state
