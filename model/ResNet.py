import torch


class Projection(torch.nn.Module):
    def __init__(self, in_channels, out_channels, stride):
        super(Projection, self).__init__()
        if stride == 1 and in_channels == out_channels:
            self.projection = lambda input: input
        else:
            kernel_size = 1 if stride == 1 else 3
            self.projection = torch.nn.Sequential(
                    torch.nn.Conv2d(
                    in_channels=in_channels,
                    out_channels=out_channels,
                    kernel_size=kernel_size,
                    stride=stride,
                    bias=False
                ),
                torch.nn.BatchNorm2d(num_features=out_channels)
            )

    def forward(self, input):
        return self.projection(input)


class BasicBlock(torch.nn.Module):
    @staticmethod
    def conv3x3(in_channels, out_channels, stride=1):
        """3x3 convolution with padding"""
        return torch.nn.Conv2d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=3,
            stride=stride,
            padding=1 if stride == 1 else 0,
            bias=False
        )

    def __init__(self, in_channels, out_channels, stride):
        super(BasicBlock, self).__init__()
        self.conv1 = BasicBlock.conv3x3(in_channels=in_channels, out_channels=out_channels, stride=stride)
        self.bn1 = torch.nn.BatchNorm2d(num_features=out_channels)
        self.relu = torch.nn.ReLU(inplace=True)
        self.conv2 = BasicBlock.conv3x3(in_channels=out_channels, out_channels=out_channels)
        self.bn2 = torch.nn.BatchNorm2d(num_features=out_channels)
        self.proj = Projection(in_channels=in_channels, out_channels=out_channels, stride=stride)

    def forward(self, input):
        output = self.conv1(input)
        output = self.bn1(output)
        output = self.relu(output)

        output = self.conv2(output)
        output = self.bn2(output)
        output += self.proj(input)
        output = self.relu(output)

        return output


# class BottleneckBlock(torch.nn.Module):
#     @staticmethod
#     def conv3x3(in_channels, out_channels, stride=1):
#         """3x3 convolution with padding"""
#         return torch.nn.Conv2d(
#             in_channels=in_channels,
#             out_channels=out_channels,
#             kernel_size=3,
#             stride=stride,
#             padding=1 if stride == 1 else 0,
#             bias=False
#         )
#
#     @staticmethod
#     def conv1x1(in_channels, out_channels, stride=1):
#         """1x1 convolution"""
#         return torch.nn.Conv2d(
#             in_channels=in_channels,
#             out_channels=out_channels,
#             kernel_size=1 if stride == 1 else 2,
#             stride=stride,
#             padding=0,
#             bias=False
#         )
#
#     def __init__(self, in_channels, out_channels, stride):
#         super(Block, self).__init__()
#         self.conv1 = Block.conv3x3(in_channels=in_channels, out_channels=out_channels, stride=stride)
#         self.bn1 = torch.nn.BatchNorm2d(num_features=out_channels)
#         self.relu = torch.nn.ReLU(inplace=True)
#         self.conv2 = Block.conv3x3(in_channels=out_channels, out_channels=out_channels)
#         self.bn2 = torch.nn.BatchNorm2d(num_features=out_channels)
#         self.proj = None
#         if stride != 1 or in_channels != out_channels:
#             self.proj = torch.nn.Sequential(
#                 Block.conv1x1(in_channels=in_channels, out_channels=out_channels, stride=stride),
#                 torch.nn.BatchNorm2d(num_features=out_channels),
#             )
#
#     def forward(self, input):
#         shortcut = input if self.proj is None else self.proj(input)
#
#         output = self.conv1(input)
#         output = self.bn1(output)
#         output = self.relu(output)
#
#         output = self.conv2(output)
#         output = self.bn2(output)
#         output += shortcut
#         output = self.relu(output)
#
#         return output


class ResNet(torch.nn.Module):
    def __init__(self, config):
        """Initialized the ResNet computation graph.

        :param config: List defining the number of blocks per layer which is optionally preceded by a projection of
            the feature map size and an expansion of the channel number.
        :type config: list<tuple(int, bool, bool)[(num_blocks, project, expand)]>[num_layers]
        """
        super(ResNet, self).__init__()
        in_channels = config.ResNet.in_channels
        # self.initial = torch.nn.Sequential(
        #     torch.nn.Conv2d(
        #         in_channels=in_channels,
        #         out_channels=64,
        #         kernel_size=7,
        #         stride=2,
        #         padding=0,
        #         dilation=1,
        #         bias=False
        #     ),
        #     torch.nn.BatchNorm2d(
        #         num_features=64
        #     ),
        #     torch.nn.ReLU(inplace=True),
        #     torch.nn.MaxPool2d(
        #         kernel_size=3,
        #         stride=2,
        #         padding=1,
        #         dilation=1
        #     )
        # )
        self.layers = torch.nn.ModuleList()
        in_channels, out_channels = 3, 32
        for layer_info in config.ResNet.structure:
            layer = torch.nn.ModuleList()
            for block_info in layer_info:
                layer.append(
                    BasicBlock(
                        in_channels=in_channels,
                        out_channels=out_channels,
                        stride=block_info[1]
                    )
                )
                in_channels = out_channels
            out_channels *= 2
            self.layers.append(layer)

        for module in self.modules():
            if isinstance(module, torch.nn.Conv2d):
                torch.nn.init.kaiming_normal_(
                    module.weight,
                    mode='fan_out',
                    nonlinearity='relu'
                )
            elif isinstance(module, torch.nn.BatchNorm2d):
                torch.nn.init.constant_(module.weight, 1)
                torch.nn.init.constant_(module.bias, 0)

    def forward(self, input):
        """Performs the inference through the ResNet graph.

        :param input: Input tensor.
        :type input: tensor<float>[batch_size, height, width, in_channels]

        :return layer_outputs:
        """
        # input = self.initial(input)
        outputs = list()
        for layer in self.layers:
            for block in layer:
                input = block(input)
            outputs.append(input)
        return outputs


class ResNetLoss(torch.nn.Module):
    def __init__(self, resnet, config):
        super(ResNetLoss, self).__init__()
        self.resnet = resnet

        self.avgpool = torch.nn.AdaptiveAvgPool2d(output_size=(1, 1))
        self.fc = torch.nn.Linear(
            in_features=config.ResNet.out_channels,
            out_features=config.ResNetLoss.num_classes,
            bias=True
        )
        self.criterion = torch.nn.CrossEntropyLoss(reduction='none')

    def forward(self, input, target):
        # Calculate classification logits
        batch_size = input.shape[0]
        input = self.resnet(input)[-1]
        input = self.avgpool(input)
        input = input.view(batch_size, -1)
        input = self.fc(input)

        # Calculate loss
        loss = self.criterion(input, target)

        # Calculate accuracy
        target = torch.unsqueeze(target, 1)
        top1 = torch.topk(input=input, k=1, dim=1, largest=True, sorted=True)[1]
        acc1 = torch.sum(torch.eq(top1, target).float(), dim=1, keepdim=False)
        top5 = torch.topk(input=input, k=5, dim=1, largest=True, sorted=True)[1]
        acc5 = torch.sum(torch.eq(top5, target).float(), dim=1, keepdim=False)

        return loss, acc1, acc5
