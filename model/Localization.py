import torch
import model.DarkNet
import model.RPNHead
import model.RPN


class Localization(torch.nn.Module):
    def __init__(self, config):
        super(Localization, self).__init__()
        self.darknet = model.DarkNet.Network(config=config)
        self.rpn_head = model.RPNHead.MultiRPNHead(config=config)
        self.rpn = model.RPN.RPN(config=config)

    def forward(self, search_patch, exemplar_patch):
        search_features = self.darknet(search_patch)
        exemplar_features = self.darknet(exemplar_patch)
        score_logits, box_deltas = self.rpn_head(search_features, exemplar_features)
        scores, boxes = self.rpn(score_logits, box_deltas)
        return scores, boxes


class LocalizationLoss(torch.nn.Module):
    def __init__(self, localization, config):
        super(LocalizationLoss, self).__init__()
        self.localization = localization

        self.rpn_loss = model.RPN.RPNLoss(rpn=localization.rpn, config=config)

    def forward(self, search_patch, exemplar_patch, target):
        search_features = self.localization.darknet(search_patch)
        exemplar_features = self.localization.darknet(exemplar_patch)
        score_logits, box_deltas = self.localization.rpn_head(search_features, exemplar_features)
        bg_score_loss, fg_score_loss, fg_box_loss = self.rpn_loss(score_logits, box_deltas, target)
        return bg_score_loss, fg_score_loss, fg_box_loss
