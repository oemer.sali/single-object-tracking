import torch

# mIOU: 61.5576%
class XZNet(torch.nn.Module):
    """Modified version of AlexNet where groups from layer2 and layer4 are removed.
    """
    def __init__(self, in_channels):
        super(XZNet, self).__init__()
        self.layer1 = torch.nn.Sequential(
            torch.nn.Conv2d(
                in_channels=in_channels,
                out_channels=96,
                kernel_size=11,
                stride=2,
                padding=0,
                dilation=1,
                groups=1,
                bias=True
            ),
            torch.nn.BatchNorm2d(
                num_features=96
            ),
            torch.nn.ReLU(inplace=True),
            torch.nn.MaxPool2d(
                kernel_size=3,
                stride=2,
                padding=0,
                dilation=1,
                return_indices=False,
                ceil_mode=False
            )
        )
        self.layer2 = torch.nn.Sequential(
            torch.nn.Conv2d(
                in_channels=96,
                out_channels=256,
                kernel_size=5,
                stride=1,
                padding=0,
                dilation=1,
                groups=1,
                bias=True
            ),
            torch.nn.BatchNorm2d(
                num_features=256
            ),
            torch.nn.ReLU(inplace=True),
            torch.nn.MaxPool2d(
                kernel_size=3,
                stride=2,
                padding=0,
                dilation=1,
                return_indices=False,
                ceil_mode=False
            )
        )
        self.layer3 = torch.nn.Sequential(
            torch.nn.Conv2d(
                in_channels=256,
                out_channels=384,
                kernel_size=3,
                stride=1,
                padding=0,
                dilation=1,
                groups=1,
                bias=True
            ),
            torch.nn.BatchNorm2d(
                num_features=384
            ),
            torch.nn.ReLU(inplace=True)
        )
        self.layer4 = torch.nn.Sequential(
            torch.nn.Conv2d(
                in_channels=384,
                out_channels=384,
                kernel_size=3,
                stride=1,
                padding=0,
                dilation=1,
                groups=1,
                bias=True
            ),
            torch.nn.BatchNorm2d(
                num_features=384
            ),
            torch.nn.ReLU(inplace=True)
        )
        self.layer5 = torch.nn.Sequential(
            torch.nn.Conv2d(
                in_channels=384,
                out_channels=256,
                kernel_size=3,
                stride=1,
                padding=0,
                dilation=1,
                groups=1,
                bias=True
            ),
            torch.nn.BatchNorm2d(
                num_features=256
            )
        )

    def forward(self, input):
        layer1 = self.layer1(input)
        layer2 = self.layer2(layer1)
        layer3 = self.layer3(layer2)
        layer4 = self.layer4(layer3)
        layer5 = self.layer5(layer4)
        return layer5
