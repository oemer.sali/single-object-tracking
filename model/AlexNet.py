import torch


class AlexNetLegacy(torch.nn.Module):
    configs = [3, 96, 256, 384, 384, 256]

    def __init__(self, width_mult=1):
        configs = list(map(lambda x: 3 if x == 3 else
                       int(x*width_mult), AlexNet.configs))
        super(AlexNetLegacy, self).__init__()
        self.features = torch.nn.Sequential(
            torch.nn.Conv2d(configs[0], configs[1], kernel_size=11, stride=2),
            torch.nn.BatchNorm2d(configs[1]),
            torch.nn.MaxPool2d(kernel_size=3, stride=2),
            torch.nn.ReLU(inplace=True),
            torch.nn.Conv2d(configs[1], configs[2], kernel_size=5),
            torch.nn.BatchNorm2d(configs[2]),
            torch.nn.MaxPool2d(kernel_size=3, stride=2),
            torch.nn.ReLU(inplace=True),
            torch.nn.Conv2d(configs[2], configs[3], kernel_size=3),
            torch.nn.BatchNorm2d(configs[3]),
            torch.nn.ReLU(inplace=True),
            torch.nn.Conv2d(configs[3], configs[4], kernel_size=3),
            torch.nn.BatchNorm2d(configs[4]),
            torch.nn.ReLU(inplace=True),
            torch.nn.Conv2d(configs[4], configs[5], kernel_size=3),
            torch.nn.BatchNorm2d(configs[5]),
        )
        self.feature_size = configs[5]

    def forward(self, x):
        x = self.features(x)
        return x


class AlexNet(torch.nn.Module):
    configs = [3, 96, 256, 384, 384, 256]

    def __init__(self, width_mult=1):
        configs = list(map(lambda x: 3 if x == 3 else
                       int(x*width_mult), AlexNet.configs))
        super(AlexNet, self).__init__()
        self.layer1 = torch.nn.Sequential(
            torch.nn.Conv2d(configs[0], configs[1], kernel_size=11, stride=2),
            torch.nn.BatchNorm2d(configs[1]),
            torch.nn.MaxPool2d(kernel_size=3, stride=2),
            torch.nn.ReLU(inplace=True),
            )
        self.layer2 = torch.nn.Sequential(
            torch.nn.Conv2d(configs[1], configs[2], kernel_size=5),
            torch.nn.BatchNorm2d(configs[2]),
            torch.nn.MaxPool2d(kernel_size=3, stride=2),
            torch.nn.ReLU(inplace=True),
            )
        self.layer3 = torch.nn.Sequential(
            torch.nn.Conv2d(configs[2], configs[3], kernel_size=3),
            torch.nn.BatchNorm2d(configs[3]),
            torch.nn.ReLU(inplace=True),
            )
        self.layer4 = torch.nn.Sequential(
            torch.nn.Conv2d(configs[3], configs[4], kernel_size=3),
            torch.nn.BatchNorm2d(configs[4]),
            torch.nn.ReLU(inplace=True),
            )

        self.layer5 = torch.nn.Sequential(
            torch.nn.Conv2d(configs[4], configs[5], kernel_size=3),
            torch.nn.BatchNorm2d(configs[5]),
            )
        self.feature_size = configs[5]

    def forward(self, x):
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        x = self.layer5(x)
        return x
