import torch


class RPNHead(torch.nn.Module):
    def __init__(self, config):
        super(RPNHead, self).__init__()
        self.num_anchors = config.RPNHead.num_anchors

        self.adjust = torch.nn.Sequential(
            torch.nn.Conv2d(
                in_channels=config.RPNHead.input_channels,
                out_channels=config.RPNHead.adjust_channels,
                kernel_size=3,
                stride=1,
                padding=1,
                dilation=1,
                groups=1,
                bias=False
            ),
            torch.nn.ReLU(inplace=True)
        )
        self.score_head = torch.nn.Conv2d(
            in_channels=config.RPNHead.adjust_channels,
            out_channels=2 * config.RPNHead.num_anchors,
            kernel_size=1,
            stride=1,
            padding=0,
            dilation=1,
            groups=1,
            bias=True
        )
        self.box_head = torch.nn.Conv2d(
            in_channels=config.RPNHead.adjust_channels,
            out_channels=4 * config.RPNHead.num_anchors,
            kernel_size=1,
            stride=1,
            padding=0,
            dilation=1,
            groups=1,
            bias=True
        )

    def forward(self, input):
        """Calculates the score logits and box deltas for the RPN module.

        :param input: Backbone feature map.
        :type input: tensor<float>[batch_size, in_channels, height, width]

        :return score_logits: Score logits.
        :rtype score_logits: tensor<float>[batch_size, (bg_logit, fg_logit), num_anchors, height, width]

        :return box_deltas: Box deltas.
        :rtype box_deltas: tensor<float>[batch_size, (dy, dx, dh, dw), num_anchors, height, width]
        """
        batch_size, _, height, width = input.shape
        adjust = self.adjust(input)
        score_logits = self.score_head(adjust).view(batch_size, 2, self.num_anchors, height, width)
        box_deltas = self.box_head(adjust).view(batch_size, 4, self.num_anchors, height, width)
        return score_logits, box_deltas


class SiameseRPNHead(torch.nn.Module):
    class _Branch(torch.nn.Module):
        def __init__(self, input_channels, adjust_channels, num_anchors, anchor_size):
            super(SiameseRPNHead._Branch, self).__init__()
            self.num_anchors = num_anchors
            self.anchor_size = anchor_size

            self.exemplar_adjust = torch.nn.Sequential(
                torch.nn.Conv2d(
                    in_channels=input_channels,
                    out_channels=adjust_channels,
                    kernel_size=3,
                    stride=1,
                    padding=1,
                    dilation=1,
                    groups=1,
                    bias=False
                ),
                torch.nn.BatchNorm2d(
                    num_features=adjust_channels
                )
            )
            self.search_adjust = torch.nn.Sequential(
                torch.nn.Conv2d(
                    in_channels=input_channels,
                    out_channels=adjust_channels,
                    kernel_size=3,
                    stride=1,
                    padding=1,
                    dilation=1,
                    groups=1,
                    bias=False
                ),
                torch.nn.BatchNorm2d(
                    num_features=adjust_channels
                )
            )
            self.head = torch.nn.Sequential(
                torch.nn.Conv2d(
                    in_channels=adjust_channels,
                    out_channels=adjust_channels,
                    kernel_size=1,
                    stride=1,
                    padding=0,
                    dilation=1,
                    groups=1,
                    bias=False
                ),
                torch.nn.BatchNorm2d(
                    num_features=adjust_channels
                ),
                torch.nn.ReLU(inplace=True),
                torch.nn.Conv2d(
                    in_channels=adjust_channels,
                    out_channels=anchor_size * num_anchors,
                    kernel_size=1,
                    stride=1,
                    padding=0,
                    dilation=1,
                    groups=1,
                    bias=True
                )
            )

        def forward(self, search, exemplar):
            """Calculates the one branch output of the Siamese RPN Head.

            :param search: Backbone feature map of the search path.
            :type search: tensor<float>[batch_size, in_channels, height, width]

            :param exemplar: Backbone feature map of the exemplar patch.
            :type exemplar: tensor<float>[batch_size, in_channels, height, width]

            :return head: Branch output of the Siamese RPN Head.
            :rtype head: tensor<float>[batch_size, anchor_size, num_anchors, height, width]
            """
            search = self.search_adjust(search)
            exemplar = self.exemplar_adjust(exemplar)

            batch_size, adjust_channels = search.shape[0:2]
            search_height, search_width = search.shape[2:4]
            exemplar_height, exemplar_width = exemplar.shape[2:4]
            exemplar = exemplar.view(batch_size, adjust_channels, 1, exemplar_height, exemplar_width)
            response = self._depthwise_conv2d(
                input=search,
                weight=exemplar,
                bias=None,
                stride=(1, 1),
                padding=(exemplar_height // 2, exemplar_width // 2),
                dilation=(1, 1),
                groups=adjust_channels
            )
            head = self.head(response).view(batch_size, self.anchor_size, self.num_anchors, search_height, search_width)
            return head

        @staticmethod
        def _depthwise_conv2d(input, weight, bias, stride, padding, dilation, groups):
            """Fast batchwise convolution of input tensors with their respective weight tensors.

            :param input: Batch of input tensors.
            :type input: tensor<float>[batch_size, input_channels, input_height, input_width]

            :param weight: Batch of kernel tensors.
            :type weight: tensor<float>[batch_size, output_channels, input_channels/groups, kernel_height, kernel_width]

            :param bias: Optional bias tensor.
            :type bias: tensor<float>[batch_size, output_channels] or None

            :param stride: Common stride of all convolution kernels.
            :type stride: tuple<int>[(stride_height, stride_width)] or int

            :param padding: Common zero padding on both sides if the input.
            :param padding: tuple<int>[(padding_height, padding_width)] or int

            :param dilation: Common spacing between kernel elements.
            :type dilation: tuple<int>[(dilation_height, dilation_width)] or int

            :param groups: Split in_channels and out_channels into groups.
            :type groups: int

            :return response: Output response map.
            :rtype response: tensor<float>[batch_size, output_channels, result_height, result_width]
            """
            batch_size = input.shape[0]
            input = input.view(torch.Size([1, -1]) + input.shape[2:4])
            weight = weight.view(torch.Size([-1]) + weight.shape[2:5])
            # print('INPUT', input.shape)
            # print('WEIGHT', weight.shape)
            response = torch.nn.functional.conv2d(
                input=input,
                weight=weight,
                bias=bias,
                stride=stride,
                padding=padding,
                dilation=dilation,
                groups=batch_size * groups
            )
            response = response.view(torch.Size([batch_size, -1]) + response.shape[2:4])
            # print('RESPONSE', response.shape)
            return response

    def __init__(self, config):
        super(SiameseRPNHead, self).__init__()
        self.score_head = self._Branch(
            input_channels=config.SiameseRPNHead.input_channels,
            adjust_channels=config.SiameseRPNHead.adjust_channels,
            num_anchors=config.SiameseRPNHead.num_anchors,
            anchor_size=2
        )
        self.box_head = self._Branch(
            input_channels=config.SiameseRPNHead.input_channels,
            adjust_channels=config.SiameseRPNHead.adjust_channels,
            num_anchors=config.SiameseRPNHead.num_anchors,
            anchor_size=4
        )

    def forward(self, search, exemplar):
        """Calculates the score logits and box deltas for the RPN module.

        :param search: Backbone feature map of the search path.
        :type search: tensor<float>[batch_size, in_channels, height, width]

        :param exemplar: Backbone feature map of the exemplar patch.
        :type exemplar: tensor<float>[batch_size, in_channels, height, width]

        :return score_logits: Score logits.
        :rtype score_logits: tensor<float>[batch_size, (bg_logit, fg_logit), num_anchors, height, width]

        :return box_deltas: Box deltas.
        :rtype box_deltas: tensor<float>[batch_size, (dy, dx, dh, dw), num_anchors, height, width]
        """
        score_logits = self.score_head(search, exemplar)
        box_deltas = self.box_head(search, exemplar)
        return score_logits, box_deltas


class MultiRPNHead(torch.nn.Module):
    class _Branch(torch.nn.Module):
        def __init__(self, input_channels, output_channels):
            super(MultiRPNHead._Branch, self).__init__()

            self.exemplar_adjust = torch.nn.Sequential(
                torch.nn.Conv2d(
                    in_channels=input_channels,
                    out_channels=output_channels,
                    kernel_size=3,
                    stride=1,
                    padding=1,
                    dilation=1,
                    groups=1,
                    bias=False
                ),
                torch.nn.BatchNorm2d(
                    num_features=output_channels
                ),
                torch.nn.ReLU(
                    inplace=True
                )
            )
            self.search_adjust = torch.nn.Sequential(
                torch.nn.Conv2d(
                    in_channels=input_channels,
                    out_channels=output_channels,
                    kernel_size=3,
                    stride=1,
                    padding=1,
                    dilation=1,
                    groups=1,
                    bias=False
                ),
                torch.nn.BatchNorm2d(
                    num_features=output_channels
                ),
                torch.nn.ReLU(
                    inplace=True
                )
            )
            self.response_adjust = torch.nn.Sequential(
                torch.nn.Conv2d(
                    in_channels=output_channels,
                    out_channels=output_channels,
                    kernel_size=1,
                    stride=1,
                    padding=0,
                    bias=False
                ),
                torch.nn.BatchNorm2d(
                    num_features=output_channels
                ),
                torch.nn.ReLU(
                    inplace=True
                ),
                torch.nn.Conv2d(
                    in_channels=output_channels,
                    out_channels=output_channels,
                    kernel_size=1,
                    stride=1,
                    padding=0,
                    bias=False
                )
            )

        def forward(self, search, exemplar):
            """Calculates the one branch output of the Siamese RPN Head.

            :param search: Backbone feature map of the search path.
            :type search: tensor<float>[batch_size, in_channels, height, width]

            :param exemplar: Backbone feature map of the exemplar patch.
            :type exemplar: tensor<float>[batch_size, in_channels, height, width]

            :return head: Branch output of the Siamese RPN Head.
            :rtype head: tensor<float>[batch_size, anchor_size, num_anchors, height, width]
            """
            search = self.search_adjust(search)
            exemplar = self.exemplar_adjust(exemplar)

            batch_size, adjust_channels = search.shape[0:2]
            # search_height, search_width = search.shape[2:4]
            exemplar_height, exemplar_width = exemplar.shape[2:4]
            exemplar = exemplar.view(batch_size, adjust_channels, 1, exemplar_height, exemplar_width)
            response = self._depthwise_conv2d(
                input=search,
                weight=exemplar,
                bias=None,
                stride=(1, 1),
                padding=(exemplar_height // 2, exemplar_width // 2),
                dilation=(1, 1),
                groups=adjust_channels
            )
            response = self.response_adjust(response)
            return response

        @staticmethod
        def _depthwise_conv2d(input, weight, bias, stride, padding, dilation, groups):
            """Fast batchwise convolution of input tensors with their respective weight tensors.

            :param input: Batch of input tensors.
            :type input: tensor<float>[batch_size, input_channels, input_height, input_width]

            :param weight: Batch of kernel tensors.
            :type weight: tensor<float>[batch_size, output_channels, input_channels/groups, kernel_height, kernel_width]

            :param bias: Optional bias tensor.
            :type bias: tensor<float>[batch_size, output_channels] or None

            :param stride: Common stride of all convolution kernels.
            :type stride: tuple<int>[(stride_height, stride_width)] or int

            :param padding: Common zero padding on both sides if the input.
            :param padding: tuple<int>[(padding_height, padding_width)] or int

            :param dilation: Common spacing between kernel elements.
            :type dilation: tuple<int>[(dilation_height, dilation_width)] or int

            :param groups: Split in_channels and out_channels into groups.
            :type groups: int

            :return response: Output response map.
            :rtype response: tensor<float>[batch_size, output_channels, result_height, result_width]
            """
            batch_size = input.shape[0]
            input = input.view(torch.Size([1, -1]) + input.shape[2:4])
            weight = weight.view(torch.Size([-1]) + weight.shape[2:5])
            # print('INPUT', input.shape)
            # print('WEIGHT', weight.shape)
            response = torch.nn.functional.conv2d(
                input=input,
                weight=weight,
                bias=bias,
                stride=stride,
                padding=padding,
                dilation=dilation,
                groups=batch_size * groups
            )
            response = response.view(torch.Size([batch_size, -1]) + response.shape[2:4])
            # print('RESPONSE', response.shape)
            return response

    def __init__(self, config):
        super(MultiRPNHead, self).__init__()
        self.num_anchors = config.SiameseRPNHead.num_anchors

        self.branch0 = self._Branch(
            input_channels=1024,
            output_channels=512
        )
        self.upsample0 = torch.nn.Sequential(
            torch.nn.ReLU(
                inplace=True
            ),
            torch.nn.ConvTranspose2d(
                in_channels=512,
                out_channels=256,
                kernel_size=3,
                stride=2,
                padding=0,
                bias=True
            )
        )
        self.branch1 = self._Branch(
            input_channels=512,
            output_channels=256
        )
        self.upsample1 = torch.nn.Sequential(
            torch.nn.ReLU(
                inplace=True
            ),
            torch.nn.ConvTranspose2d(
                in_channels=256,
                out_channels=256,
                kernel_size=3,
                stride=2,
                padding=0,
                bias=True
            )
        )
        self.branch2 = self._Branch(
            input_channels=256,
            output_channels=256
        )
        self.upsample2 = torch.nn.Sequential(
            torch.nn.ReLU(
                inplace=True
            ),
        )
        self.score_head = torch.nn.Sequential(
            torch.nn.Conv2d(
                in_channels=256,
                out_channels=256,
                kernel_size=3,
                stride=1,
                padding=1,
                bias=False
            ),
            torch.nn.BatchNorm2d(
                num_features=256
            ),
            torch.nn.ReLU(
                inplace=True
            ),
            torch.nn.Conv2d(
                in_channels=256,
                out_channels=256,
                kernel_size=3,
                stride=1,
                padding=1,
                bias=False
            ),
            torch.nn.BatchNorm2d(
                num_features=256
            ),
            torch.nn.ReLU(
                inplace=True
            ),
            torch.nn.Conv2d(
                in_channels=256,
                out_channels=256,
                kernel_size=3,
                stride=1,
                padding=1,
                bias=False
            ),
            torch.nn.BatchNorm2d(
                num_features=256
            ),
            torch.nn.ReLU(
                inplace=True
            ),
            torch.nn.Conv2d(
                in_channels=256,
                out_channels=config.SiameseRPNHead.num_anchors * 2,
                kernel_size=1,
                stride=1,
                padding=0,
                dilation=1,
                groups=1,
                bias=True
            )
        )

        self.box_head = torch.nn.Sequential(
            torch.nn.Conv2d(
                in_channels=256,
                out_channels=256,
                kernel_size=3,
                stride=1,
                padding=1,
                bias=False
            ),
            torch.nn.BatchNorm2d(
                num_features=256
            ),
            torch.nn.ReLU(
                inplace=True
            ),
            torch.nn.Conv2d(
                in_channels=256,
                out_channels=256,
                kernel_size=3,
                stride=1,
                padding=1,
                bias=False
            ),
            torch.nn.BatchNorm2d(
                num_features=256
            ),
            torch.nn.ReLU(
                inplace=True
            ),
            torch.nn.Conv2d(
                in_channels=256,
                out_channels=256,
                kernel_size=3,
                stride=1,
                padding=1,
                bias=False
            ),
            torch.nn.BatchNorm2d(
                num_features=256
            ),
            torch.nn.ReLU(
                inplace=True
            ),
            torch.nn.Conv2d(
                in_channels=256,
                out_channels=config.SiameseRPNHead.num_anchors * 4,
                kernel_size=1,
                stride=1,
                padding=0,
                dilation=1,
                groups=1,
                bias=True
            )
        )

    def forward(self, search, exemplar):
        """Calculates the score logits and box deltas for the RPN module.

        :param search: Backbone feature map of the search path.
        :type search: tensor<float>[batch_size, in_channels, height, width]

        :param exemplar: Backbone feature map of the exemplar patch.
        :type exemplar: tensor<float>[batch_size, in_channels, height, width]

        :return score_logits: Score logits.
        :rtype score_logits: tensor<float>[batch_size, (bg_logit, fg_logit), num_anchors, height, width]

        :return box_deltas: Box deltas.
        :rtype box_deltas: tensor<float>[batch_size, (dy, dx, dh, dw), num_anchors, height, width]
        """
        batch_size = search[0].shape[0]
        branch0 = self.branch0(search[-1], exemplar[-1])
        branch1 = self.branch1(search[-2], exemplar[-2])
        branch2 = self.branch2(search[-3], exemplar[-3])

        td1 = self.upsample0(branch0)
        td2 = self.upsample1(branch1 + td1)
        td3 = self.upsample2(branch2 + td2)
        response = td3


        search_height, search_width = response.shape[-2], response.shape[-1]
        score_logits = self.score_head(response).view(batch_size, 2, self.num_anchors, search_height, search_width)
        box_deltas = self.box_head(response).view(batch_size, 4, self.num_anchors, search_height, search_width)

        return score_logits, box_deltas
