import torch


class RNNCell(torch.nn.Module):
    def __init__(self, input_features, state_features, bias=True):
        super(RNNCell, self).__init__()
        self.hidden_state = torch.nn.Sequential(
            torch.nn.Linear(
                in_features=input_features + state_features,
                out_features=input_features + state_features,
                bias=bias
            ),
            torch.nn.ReLU(),
            torch.nn.Linear(
                in_features=input_features + state_features,
                out_features=state_features,
                bias=bias
            )
        )

    def forward(self, state, input):
        controller = torch.cat(tensors=[state, input], dim=1)
        hidden_state = self.hidden_state(controller)
        return hidden_state
