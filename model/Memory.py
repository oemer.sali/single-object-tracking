import torch


class Memory():
    def __init__(self, batch_size, num_memory_slots, memory_slot_size, dtype=torch.float32, device=None):
        """Initializes a new memory with zero.

        :param batch_size: scalar. Batch size.
        :param num_memory_slots: scalar. Number of memory slots.
        :param memory_slot_size: scalar. Dimension of memory slots.
        :param dtype: dtype. Data type of memory content.
        :param device: device. Device on which the memory is placed.
        """
        self.batch_size = batch_size
        self.num_memory_slots = num_memory_slots
        self.memory_slot_size = memory_slot_size
        self.dtype = dtype
        self.device = device

        # [batch_size, num_memory_slots, memory_slot_size]
        self.memory = torch.zeros(batch_size, num_memory_slots, memory_slot_size, dtype=dtype, device=device)
        # [batch_size, num_memory_slots]
        self.reliability = torch.zeros(batch_size, num_memory_slots, dtype=dtype, device=device)

    def content_address(self, key_vector):
        """Generates slot weights for content based addressing.

        :param key_vector: [batch_size, memory_slot_size]. Key vector that is compared to all memory slots.

        :return slot_weights: [batch_size, memory_slot_size]. Normalized slot weighting for memory access.
        """
        # [batch_size, num_memory_slots, memory_slot_size]
        key_vector = torch.unsqueeze(input=key_vector, dim=1).repeat(1, self.num_memory_slots, 1)
        # [batch_size, num_memory_slots]
        similarity = self.reliability*torch.nn.functional.cosine_similarity(x1=self.memory, x2=key_vector, dim=2)
        slot_weights = torch.nn.functional.softmax(input=similarity, dim=1, dtype=self.dtype)
        return slot_weights

    # def content_address(self, key_vector, key_strength):
    #     """Generates slot weights for content based addressing.
    #
    #     :param key_vector: [batch_size, memory_slot_size]. Key vector that is compared to all memory slots.
    #     :param key_strength: [batch_size]. Temperature that amplifies or attenuates the precision of the focus.
    #
    #     :return slot_weights: [batch_size, memory_slot_size]. Normalized slot weighting for memory access.
    #     """
    #     # [batch_size, num_memory_slots, memory_slot_size]
    #     key_vector = torch.unsqueeze(input=key_vector, dim=1).repeat(1, self.num_memory_slots, 1)
    #     # [batch_size, 1]
    #     key_strength = torch.unsqueeze(input=key_strength, dim=1)
    #     # [batch_size, num_memory_slots]
    #     similarity = key_strength*torch.nn.functional.cosine_similarity(x1=self.memory, x2=key_vector, dim=2)
    #     slot_weights = torch.nn.functional.softmax(input=similarity, dim=1, dtype=self.dtype)
    #     return slot_weights


    def read(self, slot_weights):
        """Reads the memory content according to given weights.

        :param slot_weights: [batch_size, num_memory_slots]. Weighting of memory slots that sum to 1.

        :return read_vector: [batch_size, memory_slot_size]. Reduced memory content.
        """
        # [batch_size, num_memory_slots, 1]
        slot_weights = torch.unsqueeze(input=slot_weights, dim=2)
        read_vector = torch.sum(input=slot_weights*self.memory, dim=1, keepdim=False, dtype=self.dtype)
        return read_vector

    def write(self, slot_weights, erase_vector, add_vector, reliability):
        """Erases the weighted memory channel-wise by the erase vector and adds the new add vector.

        :param slot_weights: [batch_size, num_memory_slots]. Weighting of memory slots that sum to 1.
        :param erase_vector: [batch_size, memory_slot_size]. Channel-wise erase stengths that lie in [0, 1].
        :param add_vector: [batch_size, memory_slot_size]. New memory content that is added to the memory.
        :param reliability: [batch_size]. Reliability of new memory content.
        """
        # [batch_size, 1]
        reliability = torch.unsqueeze(input=reliability, dim=1)
        self.reliability = self.reliability*(1.0 - slot_weights)
        self.reliability = self.reliability+slot_weights*reliability

        # [batch_size, num_memory_slots, 1]
        slot_weights = torch.unsqueeze(input=slot_weights, dim=2)
        # [batch_size, 1, memory_slot_size]
        erase_vector = torch.unsqueeze(input=erase_vector, dim=1)
        # [batch_size, 1, memory_slot_size]
        add_vector = torch.unsqueeze(input=add_vector, dim=1)
        self.memory = self.memory*(1.0-slot_weights*erase_vector)
        self.memory = self.memory+slot_weights*add_vector