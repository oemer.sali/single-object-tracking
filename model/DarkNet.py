import torch


class Initial(torch.nn.Module):
    def __init__(self, num_channels):
        super(Initial, self).__init__()
        self.conv = torch.nn.Conv2d(
            in_channels=3,
            out_channels=num_channels,
            kernel_size=3,
            stride=1,
            padding=1,
            bias=False
        )
        self.bn = torch.nn.BatchNorm2d(
            num_features=num_channels
        )
        self.relu = torch.nn.LeakyReLU(
            negative_slope=0.1,
            inplace=True
        )

    def forward(self, input):
        output = self.conv(input)
        output = self.bn(output)
        output = self.relu(output)
        return output


class Projection(torch.nn.Module):
    def __init__(self, num_channels, expansion):
        super(Projection, self).__init__()
        self.conv = torch.nn.Conv2d(
            in_channels=num_channels,
            out_channels=expansion * num_channels,
            kernel_size=3,
            stride=2,
            padding=0,
            bias=False
        )
        self.bn = torch.nn.BatchNorm2d(
            num_features=expansion * num_channels
        )
        self.relu = torch.nn.LeakyReLU(
            negative_slope=0.1,
            inplace=True
        )

    def forward(self, input):
        output = self.conv(input)
        output = self.bn(output)
        output = self.relu(output)
        return output


class Block(torch.nn.Module):
    def __init__(self, num_channels, expansion):
        super(Block, self).__init__()
        self.conv1 = torch.nn.Conv2d(
            in_channels=expansion * num_channels,
            out_channels=num_channels,
            kernel_size=1,
            stride=1,
            padding=0,
            bias=False
        )
        self.bn1 = torch.nn.BatchNorm2d(
            num_features=num_channels
        )
        self.relu1 = torch.nn.LeakyReLU(
            negative_slope=0.1,
            inplace=True
        )
        self.conv2 = torch.nn.Conv2d(
            in_channels=num_channels,
            out_channels=expansion * num_channels,
            kernel_size=3,
            stride=1,
            padding=1,
            bias=False
        )
        self.bn2 = torch.nn.BatchNorm2d(
            num_features=expansion * num_channels
        )
        self.relu2 = torch.nn.LeakyReLU(
            negative_slope=0.1,
            inplace=True
        )

    def forward(self, input):
        output = self.conv1(input)
        output = self.bn1(output)
        output = self.relu1(output)

        output = self.conv2(output)
        output = self.bn2(output)
        output = self.relu2(output)

        output = output + input
        return output


class Layer(torch.nn.Module):
    def __init__(self, num_channels, num_blocks, expansion):
        super(Layer, self).__init__()
        self.projection = Projection(
            num_channels=num_channels,
            expansion=expansion
        )
        self.blocks = torch.nn.ModuleList()
        for _ in range(0, num_blocks):
            self.blocks.append(
                Block(
                    num_channels=num_channels,
                    expansion=expansion
                )
            )

    def forward(self, input):
        output = self.projection(input)
        for block in self.blocks:
            output = block(output)
        return output


class Network(torch.nn.Module):
    def __init__(self, config):
        super(Network, self).__init__()
        self.config = config

        # Build network
        num_channels = config.DarkNet.num_channels
        self.initial = Initial(num_channels=num_channels)
        self.layers = torch.nn.ModuleList()
        for num_blocks in config.DarkNet.num_blocks:
            self.layers.append(
                Layer(
                    num_channels=num_channels,
                    num_blocks=num_blocks,
                    expansion=config.DarkNet.expansion
                )
            )
            num_channels *= config.DarkNet.expansion

        # Initialize layers
        for module in self.modules():
            if isinstance(module, torch.nn.Conv2d):
                torch.nn.init.kaiming_normal_(
                    tensor=module.weight,
                    mode='fan_out',
                    nonlinearity='leaky_relu'
                )
            elif isinstance(module, torch.nn.BatchNorm2d):
                torch.nn.init.ones_(tensor=module.weight)
                torch.nn.init.zeros_(tensor=module.bias)

    def forward(self, input):
        outputs = list()
        output = self.initial(input)
        for layer in self.layers:
            output = layer(output)
            outputs.append(output)
        return outputs
