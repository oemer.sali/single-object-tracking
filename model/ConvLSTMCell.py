import torch


class ConvLSTMCell(torch.nn.Module):
    def __init__(self, input_channels, state_channels, kernel_size=3, stride=1, padding=1, dilation=1, groups=1, bias=True):
        super(ConvLSTMCell, self).__init__()
        self.forget_gate = torch.nn.Sequential(
            torch.nn.Conv2d(
                in_channels=input_channels+state_channels,
                out_channels=state_channels,
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
                dilation=dilation,
                groups=groups,
                bias=bias
            ),
            torch.nn.Sigmoid()
        )
        self.input_gate = torch.nn.Sequential(
            torch.nn.Conv2d(
                in_channels=input_channels+state_channels,
                out_channels=state_channels,
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
                dilation=dilation,
                groups=groups,
                bias=bias
            ),
            torch.nn.Sigmoid()
        )
        self.output_gate = torch.nn.Sequential(
            torch.nn.Conv2d(
                in_channels=input_channels+state_channels,
                out_channels=state_channels,
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
                dilation=dilation,
                groups=groups,
                bias=bias
            ),
            torch.nn.Sigmoid()
        )
        self.cell_state = torch.nn.Sequential(
            torch.nn.Conv2d(
                in_channels=input_channels+state_channels,
                out_channels=state_channels,
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
                dilation=dilation,
                groups=groups,
                bias=bias
            ),
            torch.nn.Tanh()
        )
        self.hidden_state = torch.nn.Tanh()

    def forward(self, state, input):
        cell_state, hidden_state = state
        controller = torch.cat(tensors=[hidden_state, input], dim=1)
        forget_gate = self.forget_gate(controller)
        input_gate = self.input_gate(controller)
        output_gate = self.output_gate(controller)
        cell_state_candidate = self.cell_state(controller)
        cell_state = forget_gate*cell_state+input_gate*cell_state_candidate
        hidden_state_candidate = self.hidden_state(cell_state)
        hidden_state = output_gate*hidden_state_candidate
        return cell_state, hidden_state
