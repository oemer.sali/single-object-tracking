import math
import torch


class RPN(torch.nn.Module):
    def __init__(self, config):
        """Generate RPN anchors for given feature map shape.

        :param rec_field: Receptive field informations of the backbone network.
        :type rec_field: tuple<tuple<float>[(length, jump, start)]>[(height, width)]

        :param scales: 1D array of num_scales scales to generate at each pixel position.
        :type scales: tuple<float>[num_scales]

        :param ratios: 1D array of num_ratios ratios to generate at each pixel position.
        :type ratios: tuple<float>[num_ratios]
        """
        super(RPN, self).__init__()
        self.window_influence = config.RPN.window_influence
        self.score_penalty = config.RPN.score_penalty

        # tensor<float>[(y, x, h, w), num_anchors, spatial_size]
        self.register_buffer('anchors', self._gen_anchors(
            shape=config.RPN.shape,
            stride=config.RPN.stride,
            start=config.RPN.start,
            scales=config.RPN.scales,
            ratios=config.RPN.ratios
        ))
        # print('Config.RPN.shape', config.RPN.shape)
        self.register_buffer('hann_window', self._gen_hann_window(
            shape=config.RPN.shape
        ))
        self.register_buffer('mask_cross', self._cross_mask(self.anchors, config.Backbone.shape, config.RPN.threshold))
        # int, int
        self.num_anchors, self.spatial_size = self.anchors.shape[1], self.anchors.shape[2]
        # print('SPATIAL_SIZE', self.spatial_size)

    def forward(self, box_updates, score_logits, box_deltas):
        """Choose maximal scoring anchor boxes and calculate the corresponding bounding box predictions.

        :param box_updates: Updated bounding box predictions in the current search patch.
        :type box_updates: tensor<float>[batch_size, (y, x, h, w)]

        :param score_logits: Score logits tensor.
        :type score_logits: tensor<float>[batch_size, (bg_logit, fg_logit), num_anchors, height, width]

        :param box_deltas: Box deltas tensor.
        :type box_deltas: tensor<float>[batch_size, (dy, dx, dh, dw), num_anchors, height, width]

        :return score_predictions: Background/Foregorund probability of the chosen anchor boxes.
        :rtype scores_predictions: tensor<float>[batch_size, (bg_prob, fg_prob)]

        :return box_predictions: Corresponding bounding box predictions.
        :rtype box_predictions: tensor<float>[batch_size, (y, x, h, w)]
        """
        # Calculate scores
        # [batch_size, 2, num_anchors, spatial_size]
        score_logits = torch.flatten(input=score_logits, start_dim=3, end_dim=4)
        # [batch_size, 4, num_anchors, spatial_size]
        box_deltas = torch.flatten(input=box_deltas, start_dim=3, end_dim=4)

        # [batch_size, 2, num_anchors, spatial_size]
        scores = torch.nn.functional.softmax(input=score_logits, dim=1)
        # [batch_size, 4, num_anchors, spatial_size]
        boxes = self._apply_distortions(anchor=self.anchors, delta=box_deltas)

        def change(r):
            return torch.max(r, 1. / r)

        def sz(w, h):
            pad = (w + h) * 0.5
            return torch.sqrt((w + pad) * (h + pad))

        box_updates = box_updates.unsqueeze(2).unsqueeze(3)

        # scale penalty
        s_c = change(sz(boxes[:, 2], boxes[:, 3]) / (sz(box_updates[:, 2], box_updates[:, 3])))
        # aspect ratio penalty
        r_c = change((boxes[:, 2] / boxes[:, 3]) / (box_updates[:, 2] / box_updates[:, 3]))
        penalty = torch.exp(-(r_c * s_c - 1.0) * self.score_penalty)

        objectness = penalty * scores[:, 1]
        objectness = (1.0 - self.window_influence) * objectness + self.window_influence * self.hann_window

        # Max score selection
        batch_index = torch.arange(objectness.shape[0])
        max_anchors, anchor_index = torch.max(input=objectness, dim=1, keepdim=False)
        spatial_index = torch.argmax(input=max_anchors, dim=1, keepdim=False)
        anchor_index = anchor_index[batch_index, spatial_index]

        scores = scores[batch_index, :, anchor_index, spatial_index]
        boxes = boxes[batch_index, :, anchor_index, spatial_index]
        anchors = self.anchors[:, anchor_index, spatial_index].transpose(0, 1)

        return scores, boxes, anchors

    @staticmethod
    def _gen_anchors(shape, stride, start, scales, ratios):
        """ Generate RPN anchors for given feature map shape.

        :param shape: Shape of the backbone feature map.
        :type shape: tuple<int>[(height, width)]

        :param stride: Total stride of the backbone network.
        :type stride: int

        :param start: Shift of the first receptive field center.
        :type start: int

        :param scales: 1D array of num_scales scales to generate at each pixel position.
        :type scales: list<float>[num_scales]

        :param ratios: 1D array of num_ratios ratios to generate at each pixel position.
        :type ratios: list<float>[num_ratios]

        :return anchors: Generated anchor boxes in the original image space.
        :rtype anchors: tensor<float>[(y, x, h, w), num_anchors, spatial_size] with
            num_anchors = num_scales * num_ratios, spatial_size = height * width
        """
        scales = torch.as_tensor(data=scales, dtype=torch.float)
        ratios = torch.as_tensor(data=ratios, dtype=torch.float)

        centers_y = torch.arange(start=0.0, end=shape[0], dtype=torch.float) * stride + start
        centers_x = torch.arange(start=0.0, end=shape[1], dtype=torch.float) * stride + start
        spatial_size = len(centers_y) * len(centers_x)

        size_y = torch.ger(input=scales, vec2=torch.sqrt(ratios))
        size_x = torch.ger(input=scales, vec2=torch.rsqrt(ratios))
        num_anchors = len(scales) * len(ratios)

        center = torch.stack(tensors=torch.meshgrid(centers_y, centers_x), dim=0)
        center = center.view(2, 1, spatial_size)
        center = center.expand(-1, num_anchors, -1)

        size = torch.stack(tensors=(size_y, size_x), dim=0)
        size = size.view(2, num_anchors, 1)
        size = size.expand(-1, -1, spatial_size)

        anchors = torch.cat(tensors=(center, size), dim=0)

        return anchors

    @staticmethod
    def _cross_mask(anchors, shape, threshold):
        """

        :param anchors: Generated anchor boxes in the original image space.
        :type anchors: tensor<float>[(y, x, h, w), num_anchors, spatial_size] with
            num_anchors = num_scales * num_ratios, spatial_size = height * width

        :param shape: Shape of the input image.
        :type shape: tuple<int>[(height, width)]
        """
        image_pointA = torch.zeros(2, 1, 1, dtype=torch.float, device=anchors.device)
        image_pointB = torch.as_tensor(data=shape, dtype=torch.float, device=anchors.device).view(2, 1, 1) - 1.0

        anchor_center, anchor_size = torch.split(tensor=anchors, split_size_or_sections=2, dim=0)
        anchor_area = torch.prod(input=anchor_size, dim=0, keepdim=False)
        anchor_size = (anchor_size - 1.0) / 2.0
        anchor_pointA = anchor_center - anchor_size
        anchor_pointB = anchor_center + anchor_size

        cross_pointA = torch.max(input=anchor_pointA, other=image_pointA)
        cross_pointB = torch.min(input=anchor_pointB, other=image_pointB)
        cross_size = cross_pointB - cross_pointA + 1.0
        cross_area = torch.prod(input=cross_size, dim=0, keepdim=False)

        iou = torch.div(cross_area, anchor_area)
        mask = torch.ge(input=iou, other=threshold)

        return mask

    @staticmethod
    def _calc_ious(anchors, target):
        """Calculate the ious between target and anchor boxes.

        :param anchors: Anchor boxes.
        :type anchors: tensor<float>[(y, x, h, w), num_anchors, spatial_size]

        :param target: Target bounding box.
        :type target: tensor<float>[batch_size, (y, x, h, w), num_anchors, spatial_size]

        :return ious: Pairwise ious of target with anchor boxes.
        :rtype ious: tensor<float>[batch_size, num_anchors, spatial_size]
        """
        batch_size, num_anchors, spatial_size = target.shape[0], anchors.shape[1], anchors.shape[2]

        anchor_center, anchor_size = torch.split(tensor=anchors, split_size_or_sections=2, dim=0)
        target_center, target_size = torch.split(tensor=target, split_size_or_sections=2, dim=1)

        anchor_area = torch.prod(input=anchor_size, dim=0, keepdim=False)
        target_area = torch.prod(input=target_size, dim=1, keepdim=False)

        # Calculate corner points
        anchor_size = (anchor_size - 1.0) / 2.0
        target_size = (target_size - 1.0) / 2.0
        anchor_pointA, anchor_pointB = anchor_center - anchor_size, anchor_center + anchor_size
        target_pointA, target_pointB = target_center - target_size, target_center + target_size

        # Calculate size of overlap
        overlap_pointA = torch.max(anchor_pointA, target_pointA)
        overlap_pointB = torch.min(anchor_pointB, target_pointB)
        overlap_size = torch.clamp(input=overlap_pointB - overlap_pointA + 1.0, min=0.0)

        # Calculate IOUs
        overlap_area = torch.prod(input=overlap_size, dim=1, keepdim=False)
        union_area = anchor_area + target_area - overlap_area
        ious = torch.div(overlap_area, union_area)

        return ious

    # @staticmethod
    # def _calc_score_penalty(shape):
    #     """Calculate the multiplicative penalty for the scores to suppress outliers.
    #
    #     :param shape:
    #     :type shape: tuple<int>[(height, width)]
    #
    #     :return penalty:
    #     :rtype penalty: tensor<float>[spatial_size]
    #     """
    #     height_penalty = torch.hann_window(
    #         window_length=shape[0],
    #         periodic=False,
    #         dtype=torch.float
    #     )
    #     width_penalty = torch.hann_window(
    #         window_length=shape[1],
    #         periodic=False,
    #         dtype=torch.float
    #     )
    #     penalty = torch.ger(height_penalty, width_penalty)
    #     penalty = penalty.view(-1)
    #
    #     return penalty

    # @staticmethod
    # def _gen_gaussian(shape, mean, variance):
    #     """Generate a Gaussian window with the given shape.
    #
    #     :param shape: Shape of the window
    #     :type shape: tuple<int>[(height, width)]
    #
    #     :param mean: Mean of the Gaussian blob.
    #     :type mean: tensor<float>[batch_size, (y, x)]
    #
    #     :param variance: Variance of the Gaussian blob.
    #     :type variance: tensor<float>[batch_size, (h, w)]
    #
    #     :return window: The Gaussian window.
    #     :rtype window: tensor<float>[batch_size, height, width]
    #     """
    #     variance = torch.pow(input=variance, exponent=2)
    #     normalization = torch.rsqrt(input=2 * math.pi * variance)
    #
    #     sample_y = torch.arange(start=0, end=shape[0], dtype=torch.float) - mean[:, 0, None]
    #     sample_y = -torch.pow(input=sample_y, exponent=2) / (2 * variance[:, 0, None])
    #     sample_y = torch.exp(input=sample_y) * normalization[:, 0, None]
    #
    #     sample_x = torch.arange(start=0, end=shape[1], dtype=torch.float) - mean[:, 1, None]
    #     sample_x = -torch.pow(input=sample_x, exponent=2) / (2 * variance[:, 1, None])
    #     sample_x = torch.exp(input=sample_x) * normalization[:, 1, None]
    #
    #     window = torch.bmm(input=torch.unsqueeze(sample_y, 2), mat2=torch.unsqueeze(sample_x, 1))
    #
    #     return window

    @staticmethod
    def _gen_hann_window(shape):
        window_y = torch.hann_window(
            window_length=shape[0],
            periodic=False,
            dtype=torch.float
        )
        window_x = torch.hann_window(
            window_length=shape[1],
            periodic=False,
            dtype=torch.float
        )
        window = torch.ger(
            input=window_y,
            vec2=window_x
        ).view(-1)
        return window

    @staticmethod
    def _apply_distortions(anchor, delta):
        """Applies the distortion deltas to the anchors to get the predicted boxes.

        :param anchor: Generated anchor boxes in the original image space.
        :type anchors: tensor<float>[(y, x, h, w), num_anchors, spatial_size]

        :param delta: Distortion delta.
        :type delta: tensor<float>[batch_size, (dy, dx, dh, dw), num_anchors, spatial_size]

        :return prediction: Predicted box.
        :rtype prediction: tensor<float>[batch_size, (y, x, h, w), num_anchors, spatial_size]
        """
        anchor_center, anchor_size = torch.split(tensor=anchor, split_size_or_sections=2, dim=0)
        delta_center, delta_size = torch.split(tensor=delta, split_size_or_sections=2, dim=1)

        prediction_center = anchor_center + delta_center * (anchor_size - 1.0)
        prediction_size = anchor_size * torch.exp(input=delta_size)

        # prediction_angle = anchor_angle + torch.atan(input=delta_angle)
        prediction = torch.cat(tensors=(prediction_center, prediction_size), dim=1)

        return prediction


class RPNLoss(torch.nn.Module):
    def __init__(self, rpn, config):
        """Generate RPN anchors for given feature map shape.

        :param thresholds: Background/Foreground thresholds for the ious during loss calculation.
        :type thresholds: tuple<float>[(bg_thresh, fg_thresh)]

        :param counts: Background/Foreground counts of anchors during loss calculation.
        :type counts: tuple<int>[(bg_count, fg_count)]
        """
        super(RPNLoss, self).__init__()
        self.rpn = rpn

        # tuple<float>[(fg_thresh, bg_thresh)], tuple<int>[(fg_count, bg_count)]
        self.thresholds, self.counts = config.RPNLoss.thresholds, config.RPNLoss.counts

        self.sums = torch.zeros(len(config.RPN.scales) * len(config.RPN.ratios)).cuda()

    def forward(self, score_logits, box_deltas, target):
        """Calculate the RPN score and box losses.

        :param score_logits: Score logits tensor.
        :type tensor<float>[batch_size, (bg_logit, fg_logit), num_anchors, height, width]

        :param box_deltas: Box deltas tensor.
        :type tensor<float>[batch_size, (dy, dx, dh, dw), num_anchors, height, width]

        :param target: Target box in the original image.
        :type target: tensor<float>[batch_size, (y, x, h, w)]

        :return score_loss: The score head loss.
        :rtype score_loss: tensor<float>[num_bg_anchors + num_fg_anchors]

        :return box_loss: The box head loss.
        :rtype box_loss: tensor<float>[num_fg_anchors]
        """
        ious = self._calc_ious(anchors=self.rpn.anchors, target=target)

        bg_index, bg_weight = self._choose_anchors(ious=ious, max_count=self.counts[0], threshold=self.thresholds[0], foreground=False)
        fg_index, fg_weight = self._choose_anchors(ious=ious, max_count=self.counts[1], threshold=self.thresholds[1], foreground=True)

        score_logits = torch.flatten(input=score_logits, start_dim=3, end_dim=4)
        box_deltas = torch.flatten(input=box_deltas, start_dim=3, end_dim=4)

        # Calculate score loss
        bg_score_logits = score_logits[bg_index[:, 0], :, bg_index[:, 1], bg_index[:, 2]]
        # bg_ious = ious[bg_index[:, 0], bg_index[:, 1], bg_index[:, 2]]
        # bg_score_target = torch.stack(tensors=(1.0 - bg_ious, bg_ious), dim=1)
        # bg_score_loss = torch.sum(-bg_score_target * torch.nn.functional.log_softmax(input=bg_score_logits), dim=1)

        bg_score_target = torch.zeros(len(bg_index), dtype=torch.long, device=score_logits.device)
        # bg_score_loss = torch.nn.functional.binary_cross_entropy_with_logits(input=bg_score_logits, target=bg_score_target, reduction='none')
        bg_score_loss = torch.nn.functional.cross_entropy(input=bg_score_logits, target=bg_score_target, reduction='none')
        bg_score_loss *= bg_weight

        fg_score_logits = score_logits[fg_index[:, 0], :, fg_index[:, 1], fg_index[:, 2]]
        # fg_ious = ious[fg_index[:, 0], fg_index[:, 1], fg_index[:, 2]]
        # fg_score_target = torch.stack(tensors=(1.0 - fg_ious, fg_ious), dim=1)
        # fg_score_loss = torch.sum(-fg_score_target * torch.nn.functional.log_softmax(input=fg_score_logits), dim=1)

        fg_score_target = torch.ones(len(fg_index), dtype=torch.long, device=score_logits.device)
        fg_score_loss = torch.nn.functional.cross_entropy(input=fg_score_logits, target=fg_score_target, reduction='none')
        fg_score_loss *= fg_weight

        # score_logits = torch.cat(tensors=[bg_score_logits, fg_score_logits], dim=0)
        # score_target = torch.cat(tensors=[bg_score_target, fg_score_target], dim=0)
        # score_loss = torch.nn.functional.cross_entropy(input=score_logits, target=score_target, reduction='none')

        # Calculate box loss
        fg_box_deltas = box_deltas[fg_index[:, 0], :, fg_index[:, 1], fg_index[:, 2]]
        fg_box_target = self._calc_distortions(
            anchor=self.rpn.anchors[:, fg_index[:, 1], fg_index[:, 2]].transpose(0, 1),
            target=target[fg_index[:, 0], :]
        )

        fg_box_loss = torch.nn.functional.smooth_l1_loss(input=fg_box_deltas, target=fg_box_target, reduction='none')
        fg_box_loss = torch.mean(input=fg_box_loss, dim=1, keepdim=False)
        fg_box_loss *= fg_weight

        # box_loss = fg_box_loss

        return bg_score_loss, fg_score_loss, fg_box_loss

    @staticmethod
    def _calc_ious(anchors, target):
        """Calculate the ious between target and anchor boxes.

        :param anchors: Anchor boxes.
        :type anchors: tensor<float>[(y, x, h, w), num_anchors, spatial_size]

        :param target: Target bounding box.
        :type target: tensor<float>[batch_size, (y, x, h, w)]

        :return ious: Pairwise ious of target with anchor boxes.
        :rtype ious: tensor<float>[batch_size, num_anchors, spatial_size]
        """
        batch_size, num_anchors, spatial_size = target.shape[0], anchors.shape[1], anchors.shape[2]

        anchors = anchors.view(1, 4, num_anchors, spatial_size)
        anchor_center, anchor_size = torch.split(tensor=anchors, split_size_or_sections=2, dim=1)
        target = target.view(batch_size, 4, 1, 1)
        target_center, target_size = torch.split(tensor=target, split_size_or_sections=2, dim=1)

        anchor_area = torch.prod(input=anchor_size, dim=1, keepdim=False)
        target_area = torch.prod(input=target_size, dim=1, keepdim=False)

        # Calculate corner points
        anchor_size = (anchor_size - 1.0) / 2.0
        target_size = (target_size - 1.0) / 2.0
        anchor_pointA, anchor_pointB = anchor_center - anchor_size, anchor_center + anchor_size
        target_pointA, target_pointB = target_center - target_size, target_center + target_size

        # Calculate size of overlap
        overlap_pointA = torch.max(anchor_pointA, target_pointA)
        overlap_pointB = torch.min(anchor_pointB, target_pointB)
        overlap_size = torch.clamp(input=overlap_pointB - overlap_pointA + 1.0, min=0.0)

        # Calculate IOUs
        overlap_area = torch.prod(input=overlap_size, dim=1, keepdim=False)
        union_area = anchor_area + target_area - overlap_area
        ious = torch.div(overlap_area, union_area)

        return ious

    # @staticmethod
    def _choose_anchors(self, ious, max_count, threshold, foreground, epsilon=1e-12):
        """Chooses at most max_count number of anchors with iou below/above threshold if background/foreground.

        :param ious: Pairwise ious of target with anchor boxes.
        :type ious: tensor<float>[batch_size, num_anchors, spatial_size]

        :param max_count: Maximal number of chosen anchors.
        :type max_count: int

        :param threshold: IOU threshold for anchor candidates.
        :type threshold: float

        :param foreground: Whether it is a lower/upper threshold.
        :type foreground: boolean

        :param epsilon: Epsilon for numerical stability.
        :type epsilon: float

        :return index: Batch/Anchor/Spatial index of chosen anchors.
        :rtype index: tensor<long>[num_indices, (batch_index, anchor_index, spatial_index)]
        """
        # TODO: There might be multiple anchor boxes with maximal IOU with the ground truth box.

        # ious = ious.clone()
        # Calculate indices
        # [batch_size, num_anchors * spatial_size]
        batch_size, num_anchors, spatial_size = ious.shape
        ious = ious.view(batch_size, num_anchors * spatial_size)

        # Always include/exclude anchor with maximum iou
        # batch_index = torch.arange(batch_size)
        # index = torch.argmax(input=ious, dim=1, keepdim=False)
        # ious[batch_index, index] = 1.0 # WARNING: Changes underlying tensor

        # Randomly choose anchors below/above the given threshold
        mask = None
        if foreground:
            maskA = torch.ge(input=ious, other=threshold)
            max_value = torch.max(input=ious, dim=1, keepdim=True)[0]
            maskB = torch.ge(input=ious-epsilon, other=max_value)
            mask = maskA | maskB
        else:
            mask = torch.le(input=ious, other=threshold)

        mask = mask & self.rpn.mask_cross.view(-1)

        # comparator = torch.ge if foreground else torch.le
        # maskA = comparator(input=ious, other=threshold)
        #
        # max_value = torch.unsqueeze(torch.max(input=ious, dim=1, keepdim=False)[0], 1)
        # maskB = torch.eq(input=ious, other=max_value)

        # mask = maskA | maskB
        # if not foreground:
        #     ious = 1.0 - ious

        index = torch.multinomial(input=mask.float() + epsilon, num_samples=max_count, replacement=False)
        mask = mask * torch.zeros_like(mask).scatter_(dim=1, index=index, value=1)
        index = mask.nonzero()
        num = torch.sum(input=mask, dim=1, keepdim=False, dtype=torch.float)
        weight = torch.reciprocal(num)

        # Split indices into batch/anchor/spatial indices
        batch_index, anchor_index = torch.split(tensor=index, split_size_or_sections=1, dim=1)
        spatial_index = torch.remainder(anchor_index, spatial_size)
        anchor_index = torch.div(anchor_index, spatial_size)
        index = torch.cat(tensors=(batch_index, anchor_index, spatial_index), dim=1)
        weight = torch.squeeze(input=weight[batch_index], dim=1)

        return index, weight

    @staticmethod
    def _calc_distortions(anchor, target, epsilon=1e-12):
        """Calculate the distortion deltas from anchors to target boxes.

        :param anchor: Anchor box.
        :type anchor: tensor<float>[batch_size, (y, x, h, w)]

        :param target: Target box.
        :type target: tensor<float>[batch_size, (y, x, h, w)]

        :param epsilon: Epsilon for numerical stability.
        :type epsilon: float.

        :return delta: Distortion delta.
        :rtype delta: tensor<float>[batch_size, (dy, dx, dh, dw)]
        """
        anchor_center, anchor_size = torch.split(tensor=anchor, split_size_or_sections=2, dim=1)
        target_center, target_size = torch.split(tensor=target, split_size_or_sections=2, dim=1)

        delta_center = (target_center - anchor_center) / (anchor_size - 1.0 + epsilon)
        delta_size = torch.log(input=(target_size + epsilon) / (anchor_size + epsilon))
        # delta_angle = torch.tan(input=target_angle - anchor_angle)
        delta = torch.cat(tensors=(delta_center, delta_size), dim=1)

        return delta
