import os
import xml.etree.ElementTree
import numpy
import cv2
from .Container import _Image, _Object
from .MetaData import _category_map, _validation_blacklist
from torch.utils.data import Dataset


class Reader(Dataset):
    def __init__(self, root_path, split, blacklist, sentinel):
        """Reads the ILSVRC_DET images into Image objects.

        :param root_path: Root path of the ILSVRC_DET dataset.
        :param mode: One of the dataset splits 'train', 'val', or 'test'.
        :param blacklist: True if blacklisted images shall be excluded.
        :param complete: True if objects shall be included for incomplete annotations.
        """
        self._path_pattern = os.path.join(root_path, '{}', 'DET', split, '{}.{}')
        image_set_path = os.path.join(root_path, 'ImageSets', 'DET', split + '.txt')
        with open(image_set_path, 'r') as image_set_file:
            self._image_names = [line.strip().split(' ')[0] for line in image_set_file]
        self._blacklist = _validation_blacklist if split == 'val' and blacklist else dict()
        self._sentinels = dict()
        if split == 'train' and sentinel:
            image_set_pattern = os.path.join(root_path, 'ImageSets', 'DET', 'train_{}.txt')
            for category in range(200):
                with open(image_set_pattern.format(category+1), 'r') as image_set_file:
                    image_names = [line.strip().split(' ') for line in image_set_file]
                self._sentinels[category] = {line[0] for line in image_names if line[1] == '0'}

    def __getitem__(self, index):
        image_name = self._image_names[index]
        image_path = self._path_pattern.format('Data', image_name, 'JPEG')
        objects = list()
        annotation_path = self._path_pattern.format('Annotations', image_name, 'xml')
        if os.path.exists(annotation_path):
            with open(annotation_path, 'r') as annotation_file:
                annotation_elem = xml.etree.ElementTree.parse(annotation_file).getroot()
            categories = set()
            blacklist = self._blacklist.get(index, set())
            for object_elem in annotation_elem.iter('object'):
                name = object_elem.find('name').text
                if name not in blacklist:
                    category = _category_map[name]
                    categories.add(category)
                    box_elem = object_elem.find('bndbox')
                    box = numpy.asarray([
                        [float(box_elem.find('xmin').text), float(box_elem.find('ymin').text)],
                        [float(box_elem.find('xmax').text), float(box_elem.find('ymax').text)]
                    ], dtype=numpy.float32)
                    object = _Object(
                        category=category,
                        box=box
                    )
                    objects.append(object)
            for category in categories:
                if image_name in self._sentinels.get(category, set()):
                    objects.append(_Object(category=category, box=None))
        frame = _Image(
            image_path=image_path,
            objects=objects
        )
        return frame

    def __len__(self):
        return len(self._image_names)
