import os
from .Container import _Image, _Object
from .MetaData import _category_map, _validation_blacklist, _validation_ground_truth
from torch.utils.data import Dataset


class Reader(Dataset):
    def __init__(self, root_path, split, blacklist):
        """Reads the ILSVRC_CLS images into Image objects.

        :param root_path: Root path of the ILSVRC_CLS dataset.
        :param split: One of the dataset splits 'train', 'val', or 'test'.
        :param blacklist: True if blacklisted images shall be excluded.
        """
        self._split = split
        self._path_pattern = os.path.join(root_path, '{}', 'CLS-LOC', split, '{}.{}')
        image_set_name = split + ('_cls' if split == 'train' else '') + '.txt'
        image_set_path = os.path.join(root_path, 'ImageSets', 'CLS-LOC', image_set_name)
        with open(image_set_path, 'r') as image_set_file:
            self._image_names = [line.strip().split(' ')[0] for line in image_set_file]
        blacklist = _validation_blacklist if split == 'val' and blacklist else []
        self._indices = [index for index in range(len(self._image_names)) if index not in blacklist]

    def __getitem__(self, index):
        index = self._indices[index]
        image_name = self._image_names[index]
        image_path = self._path_pattern.format('Data', image_name, 'JPEG')
        objects = list()
        if self._split == 'train':
            objects.append(_Object(category=_category_map[os.path.dirname(image_name)]))
        elif self._split == 'val':
            objects.append(_Object(category=_validation_ground_truth[index]))
        image = _Image(
            image_path=image_path,
            objects=objects
        )
        return image

    def __len__(self):
        return len(self._indices)
