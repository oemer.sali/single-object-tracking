from ..Container import cached_property


class Image(object):
    @cached_property
    def image(self):
        return None

    @cached_property
    def objects(self):
        return list()


class Object(object):
    @cached_property
    def id(self):
        return None

    @cached_property
    def category(self):
        return None

    @cached_property
    def box(self):
        return None

    @cached_property
    def mask(self):
        return None
