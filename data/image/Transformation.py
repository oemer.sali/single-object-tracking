from abc import ABC, abstractmethod
from copy import deepcopy
import random
import numpy
import cv2


class Transformation(ABC):
    def __call__(self, image):
        image = deepcopy(image)
        image = self._apply(image)
        return image

    @abstractmethod
    def _apply(self, image):
        pass


## GEOMETRIC TRANSFORMATIONS ##

class Flip(Transformation):
    def __init__(self, dim):
        self._dim = dim

    def _apply(self, image):
        shape = image.image.shape[1::-1]
        image.image = self._flip_image(image.image)
        image.objects = self._flip_objects(image.objects, shape)
        return image

    def _flip_image(self, image):
        return cv2.flip(src=image, flipCode=1 - self._dim)

    def _flip_objects(self, objects, shape):
        for object in objects:
            if object.box is not None:
                object.box[:, self._dim] = shape[self._dim]-object.box[:, self._dim] - 1.0
                object.box = numpy.stack([numpy.min(object.box, axis=0), numpy.max(object.box, axis=0)], axis=0)
            if object.mask is not None:
                object.mask = self._flip_image(object.mask)
        return objects


class Resize(Transformation):
    def __init__(self, new_shape, interpolation):
        self._new_shape = new_shape
        self._interpolation = interpolation

    def _apply(self, image):
        shape = image.image.shape[1::-1]
        factor = numpy.asarray(self._new_shape, dtype=numpy.float32) - 1.0
        factor /= numpy.asarray(shape, dtype=numpy.float32) - 1.0
        image.image = self._resize_image(image.image)
        image.objects = self._resize_objects(image.objects, factor)
        return image

    def _resize_image(self, image):
        return cv2.resize(src=image, dsize=self._new_shape, interpolation=self._interpolation)

    def _resize_objects(self, objects, factor):
        for object in objects:
            if object.box is not None:
                object.box *= factor
            if object.mask is not None:
                object.mask = self._resize_image(object.mask)
        return objects


class CropAndPad(Transformation):
    def __init__(self, cropping, padding, new_shape, interpolation, border, fill):
        box = numpy.stack([cropping[0] - padding[0], cropping[1] + padding[1]], axis=0)
        scale = (numpy.asarray(new_shape, dtype=numpy.float32) - 1.0) / (box[1] - box[0])
        self._matrix = numpy.asarray([
            [1.0, 0.0, 0.0],
            [0.0, 1.0, 0.0]
        ], dtype=numpy.float32) @ numpy.asarray([
            [scale[0], 0.0, 0.0],
            [0.0, scale[1], 0.0],
            [0.0, 0.0, 1.0]
        ], dtype=numpy.float32) @ numpy.asarray([
            [1.0, 0.0, -box[0][0]],
            [0.0, 1.0, -box[0][1]],
            [0.0, 0.0, 1.0]
        ], dtype=numpy.float32)
        self._new_shape = new_shape
        self._interpolation = interpolation
        self._border = border
        self._fill = fill

    def _apply(self, image):
        image.image = self._crop_and_pad_image(image.image)
        image.objects = self._crop_and_pad_objects(image.objects)
        return image

    def _crop_and_pad_image(self, image):
        return cv2.warpAffine(
            src=image,
            M=self._matrix,
            dsize=self._new_shape,
            flags=self._interpolation,
            borderMode=self._border,
            borderValue=self._fill
        )

    def _crop_and_pad_objects(self, objects):
        for object in objects:
            if object.box is not None:
                box = numpy.stack(numpy.meshgrid(object.box[:, 0], object.box[:, 1]), axis=2)
                box = numpy.reshape(cv2.transform(src=box, m=self._matrix), newshape=(-1, 2))
                object.box = numpy.stack([numpy.min(box, axis=0), numpy.max(box, axis=0)], axis=0)
            if object.mask is not None:
                object.mask = self._crop_and_pad_image(object.mask)
        return objects


class Warp(Transformation):
    def __init__(self, cropping, padding, scale, rotation, shear, translation, new_shape, interpolation, border, fill):
        self._cropping = cropping
        self._padding = padding
        self._scale = scale
        self._angle = (numpy.radians(rotation), numpy.radians(rotation + shear))
        self._translation = translation
        self._new_shape = new_shape
        self._interpolation = interpolation
        self._border = border
        self._fill = fill

    def _apply(self, image):
        box = numpy.stack([
            self._cropping[0] - self._padding[0],
            self._cropping[1] + self._padding[1]
        ], axis=0)
        shape = box[1] - box[0]
        scale = (numpy.asarray(self._new_shape, dtype=numpy.float32) - 1.0) / shape
        matrix = numpy.asarray([
            [1.0, 0.0, 0.0],
            [0.0, 1.0, 0.0]
        ], dtype=numpy.float32) @ numpy.asarray([
            [1.0, 0.0, (self._new_shape[0] - 1.0) / 2.0],
            [0.0, 1.0, (self._new_shape[1] - 1.0) / 2.0],
            [0.0, 0.0, 1.0]
        ], dtype=numpy.float32) @ numpy.asarray([
            [1.0, 0.0, self._translation[0]],
            [0.0, 1.0, self._translation[1]],
            [0.0, 0.0, 1.0]
        ], dtype=numpy.float32) @ numpy.asarray([
            [numpy.cos(self._angle[0]), numpy.sin(self._angle[1]), 0.0],
            [-numpy.sin(self._angle[0]), numpy.cos(self._angle[1]), 0.0],
            [0.0, 0.0, 1.0]
        ], dtype=numpy.float32) @ numpy.asarray([
            [self._scale[0], 0.0, 0.0],
            [0.0, self._scale[1], 0.0],
            [0.0, 0.0, 1.0]
        ], dtype=numpy.float32) @ numpy.asarray([
            [1.0, 0.0, -(self._new_shape[0] - 1.0) / 2.0],
            [0.0, 1.0, -(self._new_shape[1] - 1.0) / 2.0],
            [0.0, 0.0, 1.0]
        ], dtype=numpy.float32) @ numpy.asarray([
            [scale[0], 0.0, 0.0],
            [0.0, scale[1], 0.0],
            [0.0, 0.0, 1.0]
        ], dtype=numpy.float32) @ numpy.asarray([
            [1.0, 0.0, -box[0][0]],
            [0.0, 1.0, -box[0][1]],
            [0.0, 0.0, 1.0]
        ], dtype=numpy.float32)
        image.image = self._warp_image(image.image, matrix)
        image.objects = self._warp_objects(image.objects, matrix)
        return image

    def _warp_image(self, image, matrix):
        return cv2.warpAffine(
            src=image,
            M=matrix,
            dsize=self._new_shape,
            flags=self._interpolation,
            borderMode=self._border,
            borderValue=self._fill
        )

    def _warp_objects(self, objects, matrix):
        for object in objects:
            if object.box is not None:
                box = numpy.stack(numpy.meshgrid(object.box[:, 0], object.box[:, 1]), axis=2)
                box = numpy.reshape(cv2.transform(src=box, m=matrix), newshape=(-1, 2))
                object.box = numpy.stack([numpy.min(box, axis=0), numpy.max(box, axis=0)], axis=0)
            if object.mask is not None:
                object.mask = self._warp_image(object.mask)
        return objects


## COLOR TRANSFORMATIONS ##

class AdjustBrightness(Transformation):
    def __init__(self, brightness_factor):
        self._brightness_factor = brightness_factor

    def _apply(self, image):
        image.image = self._adjust_brightness_image(image.image)
        return image

    def _adjust_brightness_image(self, image):
        image = image.astype(numpy.float32) * self._brightness_factor
        image = image.clip(min=0, max=255)
        image = image.astype(numpy.uint8)
        return image


class AdjustContrast(Transformation):
    def __init__(self, contrast_factor):
        self._contrast_factor = contrast_factor

    def _apply(self, image):
        image.image = self._adjust_contrast_image(image.image)
        return image

    def _adjust_contrast_image(self, image):
        image = image.astype(numpy.float32)
        mean = numpy.round(cv2.cvtColor(image, cv2.COLOR_BGR2GRAY).mean())
        image = (1.0 - self._contrast_factor) * mean + self._contrast_factor * image
        image = image.clip(min=0, max=255)
        image = image.astype(numpy.uint8)
        return image


class AdjustSaturation(Transformation):
    def __init__(self, saturation_factor):
        self._saturation_factor = saturation_factor

    def _apply(self, image):
        image.image = self._adjust_saturation_image(image.image)
        return image

    def _adjust_saturation_image(self, image):
        image = image.astype(numpy.float32)
        degenerate = cv2.cvtColor(cv2.cvtColor(image, cv2.COLOR_BGR2GRAY), cv2.COLOR_GRAY2BGR)
        image = (1.0 - self._saturation_factor) * degenerate + self._saturation_factor * image
        image = image.clip(min=0, max=255)
        image = image.astype(numpy.uint8)
        return image


class AdjustHue(Transformation):
    def __init__(self, hue_factor):
        self._hue_factor = hue_factor

    def _apply(self, image):
        image.image = self._adjust_hue_image(image.image)
        return image

    def _adjust_hue_image(self, image):
        image = image.astype(numpy.uint8)
        hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV_FULL)
        hsv[..., 0] += numpy.uint8(self._hue_factor * 255)
        image = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR_FULL)
        image = image.astype(numpy.uint8)
        return image


class AdjustGamma(Transformation):
    def __init__(self, gamma, gain):
        self._gamma = gamma
        self._gain = gain

    def _apply(self, image):
        image.image = self._adjust_gamma_image(image.image)
        return image

    def _adjust_gamma_image(self, image):
        image = image.astype(numpy.float32)
        image = 255.0 * self._gain * numpy.power(image / 255.0, self._gamma)
        image = image.clip(min=0, max=255)
        image = image.astype(numpy.uint8)
        return image


class GaussianNoise(Transformation):
    def __init__(self, mean, std):
        self._mean = mean
        self._std = std

    def _apply(self, image):
        image.image = self._gaussian_noise_image(image.image)
        return image

    def _gaussian_noise_image(self, image):
        gauss = numpy.random.normal(self._mean, self._std, image.shape).astype(numpy.float32)
        image = numpy.clip((1 + gauss) * image.astype(numpy.float32), 0, 255)
        image = image.astype(numpy.uint8)
        return image


class SaltAndPepperNoise(Transformation):
    def __init__(self, threshold):
        self._threshold = threshold

    def _apply(self, image):
        image.image = self._salt_and_pepper_noise_image(image.image)
        return image

    def _salt_and_pepper_noise_image(self, image):
        noise = numpy.random.rand(*image.shape[0:2])
        image[noise < self._threshold / 2.0] = 0
        image[noise > 1.0 - self._threshold / 2.0] = 255
        image = image.astype(numpy.uint8)
        return image


class MotionBlur(Transformation):
    def __init__(self):
        pass

    def _apply(self, image):
        image.image = self._motion_blur_image(image.image)
        return image

    def _motion_blur_image(self, image):
        size = random.randrange(5, 46, 2)
        wx = numpy.random.random()
        kernel = numpy.zeros((size, size))
        kernel[:, size // 2] += 1. / size * wx
        kernel[size // 2, :] += 1. / size * (1 - wx)
        image = cv2.filter2D(src=image, ddepth=-1, kernel=kernel)
        return image


class GaussianBlur(Transformation):
    def __init__(self, size, sigma):
        self._size = size
        self._sigma = sigma

    def _apply(self, image):
        image.image = self._blur_image(image.image)
        return image

    def _blur_image(self, image):
        return cv2.GaussianBlur(
            src=image,
            ksize=(self._size, self._size),
            sigmaX=self._sigma,
            sigmaY=self._sigma
        )


class Grayscale(Transformation):
    def _apply(self, image):
        image.image = self._grayscale_image(image.image)
        return image

    def _grayscale_image(self, image):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
        return image
