import os
import json
from .Container import _Image, _Object
from torch.utils.data import Dataset


class Reader(Dataset):
    def __init__(self, root_path, split, year):
        """Reads the ILSVRC_VID videos into Video objects.

        :param root_path: Root path of the COCO dataset. Expected folder structure:
            root_path
                +-- annotations
                    +-- instances_train2017.json
                    +-- instances_val2017.json
                +-- images
                    +-- train2017
                    # +-- test2017
                    +-- val2017
                    # +-- unlabeled2017
        :param split: One of the dataset splits 'train', 'val', 'test', or'unlabeled'.
        :param year: Data publication year.
        """
        self._root_path = root_path
        self._full_split = split + str(year)
        has_annotations = split in {'train', 'val'}
        info_name = '{}_{}.json'.format('instances' if has_annotations else 'image_info', self._full_split)
        with open(os.path.join(root_path, 'annotations', info_name), 'r') as info_file:
            info = json.load(info_file)
        self._image_infos = info['images']
        self._annotations = info['annotations'] if has_annotations else list()

    def __getitem__(self, index):
        image_info = self._image_infos[index]
        image = _Image(
            image_path=os.path.join(self._root_path, 'images', self._full_split, image_info['file_name']),
            annotations=filter(lambda annotation: annotation['image_id'] == image_info['id'], self._annotations)
        )
        return image

    def __len__(self):
        return len(self._image_infos)
