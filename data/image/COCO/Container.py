import numpy
import cv2
from ..Container import cached_property, Image, Object


class _Image(Image):
    def __init__(self, image_path, annotations):
        self._image_path = image_path
        self._annotations = annotations

    @cached_property
    def image(self):
        return cv2.imread(
            filename=self._image_path,
            flags=cv2.IMREAD_COLOR
        )

    @cached_property
    def objects(self):
        objects = list()
        for annotation in self._annotations:
            box = annotation['bbox']
            box = numpy.asarray([
                [float(box[0]), float(box[1])],
                [float(box[0]) + float(box[2]) - 1.0, float(box[1]) + float(box[3]) - 1.0]
            ], dtype=numpy.float32)
            objects.append(
                _Object(
                    id=annotation['id'],
                    category=annotation['category_id'],
                    box=box
                )
            )
        return objects


class _Object(Object):
    def __init__(self, id, category, box):
        self.id = id
        self.category = category
        self.box = box
