import json
import numpy
import cv2
from ..Container import cached_property, Image, Object


class _Image(Image):
    def __init__(self, annotation_path, image_path, mask_path):
        self._annotation_path = annotation_path
        self._image_path = image_path
        self._mask_path = mask_path

    @cached_property
    def image(self):
        return cv2.imread(
            filename=self._image_path,
            flags=cv2.IMREAD_COLOR
        )

    @cached_property
    def objects(self):
        with open(self._annotation_path) as annotation_file:
            annotation = json.load(annotation_file)
        return [
            _Object(
                id=object['id'],
                category=object['category'],
                box=numpy.asarray(object['box'], dtype=numpy.float32),
                mask_path=self._mask_path
            )
            for object in annotation
        ]


class _Object(Object):
    def __init__(self, id, category, box, mask_path):
        self.id = id
        self.category = category
        self.box = box
        self._mask_path = mask_path

    @cached_property
    def mask(self):
        if self._mask_path is None:
            return None
        else:
            return cv2.imread(
                filename=self._mask_path,
                flags=cv2.IMREAD_COLOR
            )
