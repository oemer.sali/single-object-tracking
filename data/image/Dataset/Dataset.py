import os
import json
import cv2
from .Container import _Image
from torch.utils.data import Dataset


class Reader(Dataset):
    def __init__(self, root_path, split):
        self._path_pattern = os.path.join(root_path, '{}', split, '{}' + '.{}')
        self._length = len(os.listdir(os.path.join(root_path, 'annotations', split)))

    def __getitem__(self, index):
        name = str(index).zfill(10)
        return _Image(
            annotation_path=self._path_pattern.format('annotations', name, 'json'),
            image_path=self._path_pattern.format('images', name, 'jpeg'),
            mask_path=self._path_pattern.format('masks', name, 'jpeg')
        )

    def __len__(self):
        return self._length


class Writer(object):
    def __init__(self, root_path, split):
        self._index = 0
        self._annotations_path = os.path.join(root_path, 'annotations', split)
        os.makedirs(self._annotations_path)
        self._images_path = os.path.join(root_path, 'images', split)
        os.makedirs(self._images_path)
        self._masks_path = os.path.join(root_path, 'masks', split)
        os.makedirs(self._masks_path)

    def __call__(self, image):
        name = str(self._index).zfill(10)
        cv2.imwrite(filename=os.path.join(self._images_path, name + '.jpeg'), img=image.image)
        annotation = list()
        for object in image.objects:
            annotation.append({
                'id': object.id,
                'category': object.category,
                'box': None if object.box is None else object.box.tolist()
            })
            if object.mask is not None:
                cv2.imwrite(filename=os.path.join(self._masks_path, name + '.jpeg'), img=object.mask)
        with open(os.path.join(self._annotations_path, name + '.json'), 'w') as annotation_file:
            json.dump(annotation, annotation_file)
        self._index += 1
