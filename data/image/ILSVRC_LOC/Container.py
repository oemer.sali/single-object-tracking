import cv2
from ..Container import cached_property, Image, Object


class _Image(Image):
    def __init__(self, image_path, objects):
        self._image_path = image_path
        self.objects = objects

    @cached_property
    def image(self):
        return cv2.imread(
            filename=self._image_path,
            flags=cv2.IMREAD_COLOR
        )


class _Object(Object):
    def __init__(self, category, box):
        self.category = category
        self.box = box
