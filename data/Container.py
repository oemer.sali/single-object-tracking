
class cached_property(object):
    def __init__(self, function):
        self._function = function
        self._name = function.__name__

    def __get__(self, instance, owner=None):
        if instance is None:
            return self
        if self._name not in instance.__dict__:
            instance.__dict__[self._name] = self._function(instance)
        return instance.__dict__[self._name]

    def __set__(self, instance, value):
        instance.__dict__[self._name] = value

    def __delete__(self, instance):
        del instance.__dict__[self._name]
