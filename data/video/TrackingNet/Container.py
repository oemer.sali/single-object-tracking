import os
import numpy
import cv2
import itertools
from ..Container import cached_property, Video, Frame, Object


class _Video(Video):
    def __init__(self, root_path, split_name, video_name):
        self._path_pattern = os.path.join(root_path, split_name, '{}', video_name)

    @cached_property
    def frames(self):
        annotation_path = self._path_pattern.format('anno') + '.txt'
        with open(annotation_path, 'r') as annotation:
            boxes = [numpy.asarray(box.split(','), dtype=numpy.float32).reshape((2, 2)) for box in annotation]
        boxes = [numpy.asarray([box[0], box[0]+box[1]-1.0]) for box in boxes]

        frames_path = self._path_pattern.format('frames')
        length = len(os.listdir(frames_path)) if len(boxes) == 1 else len(boxes)
        image_paths = [os.path.join(frames_path, '{}.jpg'.format(i)) for i in range(length)]

        return [
            _Frame(
                image_path=image_path,
                objects=list() if box is None else [_Object(box=box)]
            )
            for image_path, box in itertools.zip_longest(image_paths, boxes)
        ]


class _Frame(Frame):
    def __init__(self, image_path, objects):
        self._image_path = image_path
        self.objects = objects

    @cached_property
    def image(self):
        return cv2.imread(
            filename=self._image_path,
            flags=cv2.IMREAD_COLOR
        )


class _Object(Object):
    def __init__(self, box):
        self.id = 0
        self.box = box
