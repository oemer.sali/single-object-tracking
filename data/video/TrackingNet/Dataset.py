import os
from .Container import _Video
from torch.utils.data import Dataset


class Reader(Dataset):
    def __init__(self, root_path, split):
        """Reads the TrackingNet videos into Video objects.

        :param root_path: Root path of the TrackingNet dataset.
        :param split: One of the dataset splits 'train', or 'test'.
        """
        self._root_path = root_path
        self._names = list()
        for split_name in sorted([name for name in os.listdir(root_path) if split.upper() in name]):
            video_names = os.listdir(os.path.join(root_path, split_name, 'frames'))
            names = [(split_name, video_name) for video_name in sorted(video_names)]
            self._names.extend(names)

    def __getitem__(self, index):
        split_name, video_name = self._names[index]
        return _Video(
            root_path=self._root_path,
            split_name=split_name,
            video_name=video_name
        )

    def __len__(self):
        return len(self._names)
