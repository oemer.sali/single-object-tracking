import os
from .Container import _Video
from .MetaData import _categories
from torch.utils.data import Dataset


class Reader(Dataset):
    def __init__(self, root_path):
        """Reads the LaSOT videos into Video objects.

        :param root_path: Root path of the LaSOT dataset.
        """
        self._root_path = root_path

    def __getitem__(self, index):
        category = _categories[index // 20]
        video_name = category + '-' + str((index % 20) + 1)
        video_path = os.path.join(self._root_path, category, video_name)
        video = _Video(video_path=video_path, category=category)
        return video

    def __len__(self):
        return 1400
