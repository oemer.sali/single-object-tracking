import os
import numpy
import cv2
from ..Container import cached_property, Video, Frame, Object


class _Video(Video):
    def __init__(self, video_path, category):
        self._video_path = video_path
        self._category = category

    @cached_property
    def frames(self):
        with open(os.path.join(self._video_path, 'groundtruth.txt'), 'r') as groundtruth_file:
            frames = list()
            for t, box in enumerate(groundtruth_file, 1):
                box = box.split(',')
                box = numpy.asarray([
                    [float(box[0]), float(box[1])],
                    [float(box[0]) + float(box[2]) - 1.0, float(box[1]) + float(box[3]) - 1.0]
                ], dtype=numpy.float32)
                object = _Object(category=self._category, box=box)
                frame = _Frame(
                    image_path=os.path.join(self._video_path, 'img', '{:08d}.jpg'.format(t)),
                    objects=[object]
                )
                frames.append(frame)
            return frames


class _Frame(Frame):
    def __init__(self, image_path, objects):
        self._image_path = image_path
        self.objects = objects

    @cached_property
    def image(self):
        return cv2.imread(
            filename=self._image_path,
            flags=cv2.IMREAD_COLOR
        )


class _Object(Object):
    def __init__(self, category, box):
        self.id = 0
        self.category = category
        self.box = box
