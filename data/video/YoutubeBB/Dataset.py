import os
from .Container import _Video
from torch.utils.data import Dataset


class Reader(Dataset):
    def __init__(self, root_path):
        """Reads the YoutubeBB videos into Video objects.

        :param root_path: Root path of the ILSVRC_VID dataset.
        """
        self._root_path = root_path
        image_set_path = os.path.join(root_path, 'ImageSets', 'Main', 'train.txt')
        with open(image_set_path, 'r') as image_set_file:
            # frame_names = [os.path.split(line.split(' ', 1)[0]) for line in image_set_file]
            frame_names = [line.rstrip().split('+') for line in image_set_file]
        frame_names = [[line[0], int(line[1]), int(line[2]), int(line[3])] for line in frame_names]
        frame_names.sort()

        self._video_names, dirname = list(), None
        for frame_name in frame_names:
            if dirname != frame_name[0:3]:
                dirname = frame_name[0:3]
                self._video_names.append((dirname, list()))
            self._video_names[-1][1].append(frame_name[3])

    def __getitem__(self, index):
        video, frame_names = self._video_names[index]
        video_name, category, id = video
        return _Video(
            root_path=self._root_path,
            video_name=video_name,
            category=category,
            id=id,
            frame_names=frame_names
        )

    def __len__(self):
        return len(self._video_names)
