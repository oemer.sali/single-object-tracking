import os
import xml.etree.ElementTree
import numpy
import cv2
from ..Container import cached_property, Video, Frame, Object


class _Video(Video):
    def __init__(self, root_path, video_name, category, id, frame_names):
        self._root_path = root_path
        self._video_name = video_name
        self._category = category
        self._id = id
        self._frame_names = frame_names

    @cached_property
    def frames(self):
        return [
            _Frame(
                root_path=self._root_path,
                video_name=self._video_name,
                category=self._category,
                id=self._id,
                frame_name=frame_name
            )
            for frame_name in self._frame_names
        ]


class _Frame(Frame):
    def __init__(self, root_path, video_name, category, id, frame_name):
        self._id = id
        self._category = category
        self._path_pattern = os.path.join(root_path, '{}', video_name + '+' + str(category) + '+' + str(id) + '+' + str(frame_name) + '.{}')

    @cached_property
    def image(self):
        return cv2.imread(
            filename=self._path_pattern.format('JPEGImages', 'jpg'),
            flags=cv2.IMREAD_COLOR
        )

    @cached_property
    def objects(self):
        annotation_path = self._path_pattern.format('Annotations', 'xml')
        with open(annotation_path, 'r') as annotation_file:
            annotation_elem = xml.etree.ElementTree.parse(annotation_file).getroot()
        object_elem = annotation_elem.find('object')
        box_elem = object_elem.find('bndbox')
        box = numpy.asarray([
            [float(box_elem.find('xmin').text), float(box_elem.find('ymin').text)],
            [float(box_elem.find('xmax').text), float(box_elem.find('ymax').text)]
        ], dtype=numpy.float32)
        object = _Object(
            id=self._id,
            category=self._category,
            box=box
        )
        return [object]


class _Object(Object):
    def __init__(self, id, category, box):
        self.id = id
        self.category = category
        self.box = box
