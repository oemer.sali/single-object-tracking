import os
import json
import cv2
from .Container import _Video
from torch.utils.data import Dataset

'''
directory structure:
- annotations
    - <split_0>
        - video_000000
            - frame_000000.json
            - frame_000001.json
            - ....
        - video_000001
            - frame_000000.json
            - frame_000001.json
            - ....
    - <split_1>
        - video_000000
            - frame_000000.json
            - frame_000001.json
            - ....
        - video_000001
            - frame_000000.json
            - frame_000001.json
            - ....
    - ...
- frames
    - <split_0>
        - video_000000
            - frame_000000.json
            - frame_000001.json
            - ....
        - video_000001
            - frame_000000.json
            - frame_000001.json
            - ....
    - <split_1>
        - video_000000
            - frame_000000.json
            - frame_000001.json
            - ....
        - video_000001
            - frame_000000.json
            - frame_000001.json
            - ....
    - ...
'''


class Reader(Dataset):
    def __init__(self, dataset_path, split):
        self._dataset_path = os.path.join(dataset_path, '{component}', split)
        self._length = len(os.listdir(self._dataset_path.format(component='annotations')))

    def __getitem__(self, index):
        video_path = os.path.join(self._dataset_path, 'video_{:06d}'.format(index))
        return _Video(video_path=video_path)

    def __len__(self):
        return self._length


class Writer(object):
    def __init__(self, dataset_path, split):
        self._video_index = 0
        self._dataset_path = os.path.join(dataset_path, '{component}', split)
        os.makedirs(self._dataset_path.format(component='annotations'))
        os.makedirs(self._dataset_path.format(component='frames'))
        # os.makedirs(self._dataset_path.format(component='masks'))

    def __call__(self, video):
        video_path = os.path.join(self._dataset_path, 'video_{:06d}'.format(self._video_index))
        os.makedirs(video_path.format(component='annotations'))
        os.makedirs(video_path.format(component='frames'))
        for frame_index, frame in enumerate(video):
            frame_path = os.path.join(video_path, 'frame_{:06d}'.format(frame_index)+'.{extension}')
            cv2.imwrite(filename=frame_path.format(component='frames', extension='jpeg'), img=frame.image)
            annotation = list()
            for object in frame:
                annotation.append({
                    'id': object.id,
                    'category': object.category,
                    'box': None if object.box is None else object.box.tolist()
                })
                # if object.mask is not None:
                #     cv2.imwrite(filename=os.path.join(self._masks_path, name + '.jpeg'), img=object.mask)
            with open(frame_path.format(component='annotations', extension='json'), 'w') as annotation_file:
                json.dump(annotation, annotation_file)
        self._video_index += 1
