import os
import json
import numpy
import cv2
from ..Container import cached_property, Video, Frame, Object


class _Video(Video):
    def __init__(self, video_path):
        # os.path.join(root_path, '{}', split, 'video_' + str(video_index).zfill(6))
        # self._frame_path = os.path.join(video_path, 'frame_{index:06d}.{extension}')
        self._video_path = video_path
        self._length = len(os.listdir(video_path.format(component='annotations')))

    @cached_property
    def frames(self):
        return [
            _Frame(frame_path=os.path.join(self._video_path, 'frame_{:06d}'.format(index) + '.{extension}'))
            for index in range(self._length)
        ]


class _Frame(Frame):
    def __init__(self, frame_path):
        self._frame_path = frame_path

    @cached_property
    def image(self):
        return cv2.imread(
            filename=self._frame_path.format(component='frames', extension='jpeg'),
            flags=cv2.IMREAD_COLOR
        )

    @cached_property
    def objects(self):
        with open(self._frame_path.format(component='annotations', extension='json')) as annotation_file:
            annotation = json.load(annotation_file)
        return [
            _Object(
                id=object['id'],
                category=object['category'],
                box=numpy.asarray(object['box'], dtype=numpy.float32),
                mask_path=None
            )
            for object in annotation
        ]


class _Object(Object):
    def __init__(self, id, category, box, mask_path):
        self.id = id
        self.category = category
        self.box = box
        self._mask_path = mask_path

    @cached_property
    def mask(self):
        if self._mask_path is None:
            return None
        else:
            return cv2.imread(
                filename=self._mask_path,
                flags=cv2.IMREAD_COLOR
            )
