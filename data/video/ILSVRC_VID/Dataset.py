import os
from .Container import _Video
from torch.utils.data import Dataset


class Reader(Dataset):
    def __init__(self, root_path, split):
        """Reads the ILSVRC_VID videos into Video objects.

        :param root_path: Root path of the ILSVRC_VID dataset.
        :param split: One of the dataset splits 'train', 'val', or 'test'.
        """
        self._root_path = root_path
        self._split = split
        image_set_path = os.path.join(root_path, 'ImageSets', 'VID', split + '.txt')
        with open(image_set_path, 'r') as image_set_file:
            frame_names = [os.path.split(line.split(' ', 1)[0]) for line in image_set_file]
        self._video_names, dirname = list(), None
        for frame_name in frame_names:
            if dirname != frame_name[0]:
                dirname = frame_name[0]
                self._video_names.append((dirname, list()))
            self._video_names[-1][1].append(frame_name[1])

    def __getitem__(self, index):
        video_name, frame_names = self._video_names[index]
        return _Video(
            root_path=self._root_path,
            split=self._split,
            video_name=video_name,
            frame_names=frame_names
        )

    def __len__(self):
        return len(self._video_names)
