import os
import xml.etree.ElementTree
import numpy
import cv2
from .MetaData import _category_map
from ..Container import cached_property, Video, Frame, Object


class _Video(Video):
    def __init__(self, root_path, split, video_name, frame_names):
        self._root_path = root_path
        self._split = split
        self._video_name = video_name
        self._frame_names = frame_names

    @cached_property
    def frames(self):
        return [
            _Frame(
                root_path=self._root_path,
                split=self._split,
                frame_name=os.path.join(self._video_name, frame_name)
            )
            for frame_name in self._frame_names
        ]


class _Frame(Frame):
    def __init__(self, root_path, split, frame_name):
        self._path_pattern = os.path.join(root_path, '{}', 'VID', split, frame_name + '.{}')

    @cached_property
    def image(self):
        return cv2.imread(
            filename=self._path_pattern.format('Data', 'JPEG'),
            flags=cv2.IMREAD_COLOR
        )

    @cached_property
    def objects(self):
        annotation_path = self._path_pattern.format('Annotations', 'xml')
        objects = list()
        if os.path.exists(annotation_path):
            with open(annotation_path, 'r') as annotation_file:
                annotation_elem = xml.etree.ElementTree.parse(annotation_file).getroot()
            for object_elem in annotation_elem.iter('object'):
                id = int(object_elem.find('trackid').text)
                category = _category_map[object_elem.find('name').text]
                box_elem = object_elem.find('bndbox')
                box = numpy.asarray([
                    [float(box_elem.find('xmin').text), float(box_elem.find('ymin').text)],
                    [float(box_elem.find('xmax').text), float(box_elem.find('ymax').text)]
                ], dtype=numpy.float32)
                object = _Object(
                    id=id,
                    category=category,
                    box=box
                )
                objects.append(object)
        return objects


class _Object(Object):
    def __init__(self, id, category, box):
        self.id = id
        self.category = category
        self.box = box
