
_splits = {
    'OTB-50': [
        'Basketball', 'Biker', 'Bird1', 'BlurBody', 'BlurCar2', 'BlurFace', 'BlurOwl', 'Bolt', 'Box', 'Car1',
        'Car4', 'CarDark', 'CarScale', 'ClifBar', 'Couple', 'Crowds', 'David', 'Deer', 'Diving', 'DragonBaby',
        'Dudek', 'Football', 'Freeman4', 'Girl', 'Human3', 'Human4', 'Human6', 'Human9', 'Ironman', 'Jump',
        'Jumping', 'Liquor', 'Matrix', 'MotorRolling', 'Panda', 'RedTeam', 'Shaking', 'Singer2', 'Skating1', 'Skating2',
        'Skiing', 'Soccer', 'Surfer', 'Sylvester', 'Tiger2', 'Trellis', 'Walking', 'Walking2', 'Woman'
    ],
    'OTB-100': [
        'Basketball', 'Biker', 'Bird1', 'Bird2', 'BlurBody', 'BlurCar1', 'BlurCar2', 'BlurCar3', 'BlurCar4', 'BlurFace',
        'BlurOwl', 'Board', 'Bolt', 'Bolt2', 'Box', 'Boy', 'Car1', 'Car2', 'Car24', 'Car4',
        'CarDark', 'CarScale', 'ClifBar', 'Coke', 'Couple', 'Coupon', 'Crossing', 'Crowds', 'Dancer', 'Dancer2',
        'David', 'David2', 'David3', 'Deer', 'Diving', 'Dog', 'Dog1', 'Doll', 'DragonBaby', 'Dudek',
        'FaceOcc1', 'FaceOcc2', 'Fish', 'FleetFace', 'Football', 'Football1', 'Freeman1', 'Freeman3', 'Freeman4', 'Girl',
        'Girl2', 'Gym', 'Human2', 'Human3', 'Human4', 'Human5', 'Human6', 'Human7', 'Human8', 'Human9',
        'Ironman', 'Jogging', 'Jump', 'Jumping', 'KiteSurf', 'Lemming', 'Liquor', 'Man', 'Matrix', 'Mhyang',
        'MotorRolling', 'MountainBike', 'Panda', 'RedTeam', 'Rubik', 'Shaking', 'Singer1', 'Singer2', 'Skater', 'Skater2',
        'Skating1', 'Skating2', 'Skiing', 'Soccer', 'Subway', 'Surfer', 'Suv', 'Sylvester', 'Tiger1', 'Tiger2',
        'Toy', 'Trans', 'Trellis', 'Twinnings', 'Vase', 'Walking', 'Walking2', 'Woman'
    ],
}

_annotation_intervals = {
    'David': (300, 770),
    'Diving': (1, 215),
    'Football1': (1, 74),
    'Freeman3': (1, 460),
    'Freeman4': (1, 283)
}
