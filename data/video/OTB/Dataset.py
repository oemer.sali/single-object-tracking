from .MetaData import _splits
from .Container import _Video
from torch.utils.data import Dataset


class Reader(Dataset):
    def __init__(self, root_path, split):
        """Reads the OTB videos into Video objects.

        :param root_path: Root path of the OTB dataset.
        :param split: One of the dataset splits 'OTB-50', or 'OTB-100'.
        """
        self._root_path = root_path
        self._video_names = _splits[split]

    def __getitem__(self, index):
        video_name = self._video_names[index]
        video = _Video(root_path=self._root_path, video_name=video_name)
        return video

    def __len__(self):
        return len(self._video_names)
