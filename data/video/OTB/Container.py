import os
import numpy
import cv2
from .MetaData import _annotation_intervals
from ..Container import cached_property, Video, Frame, Object


class _Video(Video):
    def __init__(self, root_path, video_name):
        self._video_name = video_name
        self._video_path = os.path.join(root_path, video_name)
        # Some videos are in an extra subfolder (Biker, Bird1,...)
        video_path_ext = os.path.join(self._video_path, video_name)
        if os.path.exists(video_path_ext):
            self._video_path = video_path_ext

    @cached_property
    def frames(self):
        frames_path = os.path.join(self._video_path, 'img')
        # Some img subfolders have .ini files (CarScale, Subway,...)
        frame_names = sorted([name for name in os.listdir(frames_path) if name.endswith('.jpg')])

        # Some videos have multiple ground_truth files (Jogging, Human4,...)
        annotation_names = sorted([name for name in os.listdir(self._video_path) if name.startswith('groundtruth_rect') and name.endswith('.txt')])
        # Some annotation files are empty (Human4,...)
        if self._video_name == 'Human4':
            annotation_names.pop(0)
        annotation_files = [open(os.path.join(self._video_path, name), 'r') for name in annotation_names]
        annotation_interval = _annotation_intervals.get(self._video_name, (1, len(frame_names)))

        frames = []
        for t, frame_name in enumerate(frame_names):
            objects = []
            if annotation_interval[0] <= t+1 <= annotation_interval[1]:
                for id, annotation_file in enumerate(annotation_files):
                    box = annotation_file.readline().strip()
                    if ',' in box:
                        box = box.split(',')
                    elif '\t' in box:
                        box = box.split('\t')
                    elif ' ' in box:
                        box = box.split(' ')
                    box = numpy.asarray([
                        [float(box[0]), float(box[1])],
                        [float(box[0])+float(box[2])-1.0, float(box[1])+float(box[3])-1.0]
                    ], dtype=numpy.float32)
                    objects.append(_Object(id=id, box=box))
            frames.append(
                _Frame(
                    image_path=os.path.join(frames_path, frame_name),
                    objects=objects
                )
            )

        for annotation_file in annotation_files:
            annotation_file.close()

        return frames


class _Frame(Frame):
    def __init__(self, image_path, objects):
        self._image_path = image_path
        self.objects = objects

    @cached_property
    def image(self):
        return cv2.imread(
            filename=self._image_path,
            flags=cv2.IMREAD_COLOR
        )


class _Object(Object):
    def __init__(self, id, box):
        self.id = id
        self.box = box
