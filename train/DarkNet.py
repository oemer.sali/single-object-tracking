import os
import math
import random
import logging
import argparse
import numpy
import cv2
import torch
import data.image.Transformation
import data.image.Dataset
import data.image.ILSVRC_CLS
import Config
import model.DarkNet


def parse_arguments():
    parser = argparse.ArgumentParser(description='ResNet backbone training')
    parser.add_argument('--dest', type=str, dest='dest', required=True, help='training directory path')
    parser.add_argument('--data', type=str, dest='data', required=True, help='path to training data')
    parser.add_argument('--workers', type=int, dest='workers', default=32, help='number of data loading workers')
    parser.add_argument('--epochs', type=int, dest='epochs', default=100, help='number of total epochs to run')
    parser.add_argument('--start-epoch', type=int, dest='start_epoch', default=0, help='manual epoch number')
    parser.add_argument('--batch-size', type=int, dest='batch_size', default=256, help='mini-batch size of all gpus')
    parser.add_argument('--start-lr', type=float, dest='start_lr', default=1e-1, help='inital learning rate')
    parser.add_argument('--end-lr', type=float, dest='end_lr', default=1e-7, help='final learning rate')
    parser.add_argument('--momentum', type=float, dest='momentum', default=0.9, help='momentum')
    parser.add_argument('--weight-decay', type=float, dest='weight_decay', default=5e-4, help='weight decay')
    parser.add_argument('--gpus', nargs='+', type=int, dest='gpus', default=list(range(torch.cuda.device_count())),
                        help='list of gpus to use for training')
    parser.add_argument('--resume', action='store_true', dest='resume')
    return parser.parse_args()


class TrainDataset(torch.utils.data.Dataset):
    def __init__(self, data_path):
        self._dataset = data.image.ILSVRC_CLS.Reader(root_path=data_path, split='train', blacklist=True)

    def __getitem__(self, index):
        image = self._dataset[index]
        shape = image.image.shape[1::-1]
        image.objects = random.sample(image.objects, 1)

        # Randomly crop image with scale 0.8..1.0 and ratio 3/4..4/3
        scale, ratio = random.uniform(0.8, 1.0), random.uniform(3.0 / 4.0, 4.0 / 3.0)
        area = scale * shape[0] * shape[1]
        width, height = math.sqrt(area * ratio), math.sqrt(area / ratio)
        xmin, ymin = random.uniform(0.0, shape[0] - width), random.uniform(0.0, shape[1] - height)
        xmax, ymax = xmin + width - 1.0, ymin + height - 1.0
        cropping = numpy.asarray([[xmin, ymin], [xmax, ymax]], dtype=numpy.float32)
        padding = numpy.zeros(shape=(2, 2), dtype=numpy.float32)
        mean = tuple(map(int, image.image.mean(axis=(0, 1))))

        image = data.image.Transformation.Warp(
            cropping=cropping,
            padding=padding,
            scale=(1.0, 1.0),
            rotation=random.uniform(-3.5, 3.5),
            shear=0.0,
            translation=(0.0, 0.0),
            new_shape=(255, 255),
            interpolation=cv2.INTER_CUBIC,
            border=cv2.BORDER_CONSTANT,
            fill=mean
        )(image)

        # Random horizontal flip
        if random.random() < 0.5:
            image = data.image.Transformation.Flip(dim=0)(image)

        # Random grayscale
        if random.random() < 0.25:
            image = data.image.Transformation.Grayscale()(image)

        # Random color jitter
        image = data.image.Transformation.AdjustBrightness(
            brightness_factor=random.uniform(3.0 / 4.0, 4.0 / 3.0)
        )(image)
        image = data.image.Transformation.AdjustContrast(
            contrast_factor=random.uniform(3.0 / 4.0, 4.0 / 3.0)
        )(image)
        image = data.image.Transformation.AdjustSaturation(
            saturation_factor=random.uniform(3.0 / 4.0, 4.0 / 3.0)
        )(image)
        image = data.image.Transformation.SaltAndPepperNoise(
            threshold=random.uniform(0.0, 0.03)
        )(image)

        # Convert to tensor
        target = image.objects[0].category
        image = numpy.transpose(a=image.image, axes=(2, 0, 1))
        image = torch.as_tensor(image, dtype=torch.float) / 255.0

        return image, target

    def __len__(self):
        return len(self._dataset)


class ValDataset(torch.utils.data.Dataset):
    def __init__(self, data_path):
        self._dataset = data.image.ILSVRC_CLS.Reader(root_path=data_path, split='val', blacklist=True)

    def __getitem__(self, index):
        image = self._dataset[index]
        image.objects = random.sample(image.objects, 1)

        # Resize image with shorter side to 256
        shape = image.image.shape[1::-1]
        factor = 255.0 / min(*shape)
        image = data.image.Transformation.Resize(
            new_shape=(round(factor * shape[0]), round(factor * shape[1])),
            interpolation=cv2.INTER_CUBIC
        )(image)

        # Crop center (224, 224)
        shape = image.image.shape[1::-1]
        center = (numpy.asarray(shape) - 1.0) / 2.0
        size = (numpy.asarray([255.0, 255.0], dtype=numpy.float32) - 1.0) / 2.0
        cropping = numpy.stack([center - size, center + size])
        padding = numpy.zeros(shape=(2, 2), dtype=numpy.float32)
        image = data.image.Transformation.CropAndPad(
            cropping=cropping,
            padding=padding,
            new_shape=(255, 255),
            interpolation=cv2.INTER_CUBIC,
            border=cv2.BORDER_CONSTANT,
            fill=(0, 0, 0)
        )(image)

        # Convert to tensor
        target = image.objects[0].category
        image = numpy.transpose(a=image.image, axes=(2, 0, 1))
        image = torch.as_tensor(image, dtype=torch.float) / 255.0

        return image, target

    def __len__(self):
        return len(self._dataset)


class Loss(torch.nn.Module):
    def __init__(self, darknet, config):
        super(Loss, self).__init__()
        self.darknet = darknet
        self.config = config

        self.linear = torch.nn.Conv2d(
            in_channels=config.DarkNet.num_channels * config.DarkNet.expansion ** len(config.DarkNet.num_blocks),
            out_channels=1000,
            kernel_size=1,
            stride=1,
            padding=0,
            bias=True
        )
        self.avgpool = torch.nn.AdaptiveAvgPool2d(output_size=(1, 1))
        self.criterion = torch.nn.CrossEntropyLoss(reduction='none')

    def forward(self, input, target):
        # Calculate classification logits
        batch_size = input.shape[0]
        output = self.darknet(input)[-1]
        output = self.linear(output)
        output = self.avgpool(output)
        output = output.view(batch_size, -1)

        # Calculate loss
        loss = self.criterion(output, target)

        # Calculate accuracy
        target = torch.unsqueeze(target, 1)
        top1 = torch.topk(input=output, k=1, dim=1, largest=True, sorted=True)[1]
        acc1 = torch.sum(torch.eq(top1, target).float(), dim=1, keepdim=False)
        top5 = torch.topk(input=output, k=5, dim=1, largest=True, sorted=True)[1]
        acc5 = torch.sum(torch.eq(top5, target).float(), dim=1, keepdim=False)

        return loss, acc1, acc5


class AverageMeter(object):
    def __init__(self, name):
        self.name = name
        self.reset()

    def reset(self):
        self.val = 0.0
        self.sum = 0.0
        self.count = 0.0

    def update(self, val, n):
        self.val = val
        self.sum += val * n
        self.count += n

    def __str__(self):
        return '{}[B:{:2.5f}, A:{:2.5f}]'.format(self.name, self.val, self.sum / self.count)


def train(data_loader, model, optimizer, epoch):
    loss_meter = AverageMeter('Loss')
    acc1_meter = AverageMeter('Acc@1')
    acc5_meter = AverageMeter('Acc@5')

    model.train()
    for step, (image, target) in enumerate(data_loader):
        batch_size = image.shape[0]
        image = image.cuda(non_blocking=True)
        target = target.cuda(non_blocking=True)

        # Compute training output
        loss, acc1, acc5 = model(image, target)
        loss, acc1, acc5 = torch.mean(loss, 0), torch.mean(acc1, 0), torch.mean(acc5, 0)

        # Perform gradient descent
        optimizer.zero_grad()
        loss.backward()
        # torch.nn.utils.clip_grad_norm_(module.parameters(), 5.0)
        optimizer.step()

        # Update loss and accuracy
        loss_meter.update(val=loss.item(), n=batch_size)
        acc1_meter.update(val=acc1, n=batch_size)
        acc5_meter.update(val=acc5, n=batch_size)
        logging.info('Train | Epoch[{:5d}, {:5d}] {!s} {!s} {!s}'.format(epoch, step, loss_meter, acc1_meter, acc5_meter))


def validate(data_loader, model, epoch):
    loss_meter = AverageMeter('Loss')
    acc1_meter = AverageMeter('Acc@1')
    acc5_meter = AverageMeter('Acc@5')

    model.eval()
    with torch.no_grad():
        for step, (image, target) in enumerate(data_loader):
            batch_size = image.shape[0]
            image = image.cuda(non_blocking=True)
            target = target.cuda(non_blocking=True)

            # Compute validation output
            loss, acc1, acc5 = model(image, target)
            loss, acc1, acc5 = torch.mean(loss, 0), torch.mean(acc1, 0), torch.mean(acc5, 0)

            # Update loss and accuracy
            loss_meter.update(val=loss.item(), n=batch_size)
            acc1_meter.update(val=acc1, n=batch_size)
            acc5_meter.update(val=acc5, n=batch_size)
            logging.info('Val | Epoch[{:5d}, {:5d}] {!s} {!s} {!s}'.format(epoch, step, loss_meter, acc1_meter, acc5_meter))
    return acc1_meter.sum / acc1_meter.count


if __name__ == '__main__':
    args = parse_arguments()

    # Create model and optimizer
    torch.cuda.set_device(args.gpus[0])

    model = model.DarkNet.Network(Config)
    model = Loss(model, Config)
    model = torch.nn.DataParallel(model, device_ids=args.gpus, dim=0).cuda()
    optimizer = torch.optim.SGD(model.parameters(), lr=args.start_lr, momentum=args.momentum, weight_decay=args.weight_decay)
    lr_scheduler = torch.optim.lr_scheduler.ExponentialLR(
        optimizer=optimizer,
        gamma=(args.end_lr / args.start_lr) ** (1.0 / (args.epochs - 1.0)),
    )

    # Resume from checkpoint or create new training directory
    if not os.path.exists(args.dest):
        os.makedirs(args.dest)
    logging.basicConfig(filename=os.path.join(args.dest, 'logging.log'), level=logging.DEBUG)
    logging.info(args)
    if args.resume and os.path.exists(os.path.join(args.dest, 'checkpoint.pt')):
        logging.info('Loading checkpoint "{}".'.format(os.path.join(args.dest, 'checkpoint.pt')))
        checkpoint = torch.load(os.path.join(args.dest, 'checkpoint.pt'))
        model.module.load_state_dict(checkpoint['model'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        args.start_epoch = checkpoint['epoch']

    torch.backends.cudnn.benchmark = True

    # Create data loaders
    train_data_loader = torch.utils.data.dataloader.DataLoader(
        dataset=TrainDataset(data_path=args.data),
        batch_size=args.batch_size,
        shuffle=True,
        num_workers=args.workers,
        pin_memory=True
    )
    val_data_loader = torch.utils.data.dataloader.DataLoader(
        dataset=ValDataset(data_path=args.data),
        batch_size=args.batch_size,
        shuffle=False,
        num_workers=args.workers,
        pin_memory=True
    )

    # Run training loop
    best_acc1 = 0.0
    for epoch in range(args.start_epoch, args.epochs):
        train(
            data_loader=train_data_loader,
            model=model,
            optimizer=optimizer,
            epoch=epoch
        )
        acc1 = validate(
            data_loader=val_data_loader,
            model=model,
            epoch=epoch
        )
        checkpoint = {
            'model': model.module.state_dict(),
            'optimizer': optimizer.state_dict(),
            'epoch': epoch + 1
        }
        torch.save(checkpoint, os.path.join(args.dest, 'checkpoint.pt'))
        torch.save(model.module.darknet.state_dict(), os.path.join(args.dest, 'model.pt'))
        if acc1 > best_acc1:
            torch.save(checkpoint, os.path.join(args.dest, 'best_checkpoint.pt'))
            torch.save(model.module.darknet.state_dict(), os.path.join(args.dest, 'best_model.pt'))
            best_acc1 = acc1
        lr_scheduler.step()

    logging.info('Training finished.')
