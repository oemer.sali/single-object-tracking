import torch
import model.XZNet


class XZNetTrain(torch.nn.Module):
    def __init__(self, in_channels, in_height, in_width, num_classes):
        super(XZNetTrain, self).__init__()

        self.output_size = 256 * self._calculate_size(in_height) * self._calculate_size(in_width)

        self.model = model.XZNet.XZNet(in_channels)
        self.classifier = torch.nn.Sequential(
            torch.nn.Dropout(p=0.5, inplace=True),
            torch.nn.Linear(
                in_features=self.output_size,
                out_features=4096,
                bias=True
            ),
            torch.nn.ReLU(inplace=True),
            torch.nn.Dropout(p=0.5, inplace=True),
            torch.nn.Linear(
                in_features=4096,
                out_features=4096,
                bias=True
            ),
            torch.nn.ReLU(inplace=True),
            torch.nn.Linear(
                in_features=4096,
                out_features=num_classes,
                bias=True
            )
        )
        self.loss = torch.nn.CrossEntropyLoss(
            weight=None,
            reduction='mean'
        )

    def forward(self, input, target):
        output = self.model(input)
        logits = self.classifier(output.view(input.shape[0], self.output_size))
        loss = self.loss(logits, target)
        return logits, loss

    @staticmethod
    def _calculate_size(input_size):
        layer1_size = ((input_size - 11) // 2 + 1 - 3) // 2 + 1
        layer2_size = ((layer1_size - 5) + 1 - 3) // 2 + 1
        layer3_size = (layer2_size - 3) + 1
        layer4_size = (layer3_size - 3) + 1
        layer5_size = (layer4_size - 3) + 1
        output_size = layer5_size
        return output_size
