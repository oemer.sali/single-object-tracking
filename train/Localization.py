import cv2
import math
import random
import numpy
import torch
import torch.utils.data
import os
import logging
import argparse
import Config
import data.image.Transformation
import data.image.ILSVRC_LOC
import data.image.COCO
import data.video.ILSVRC_VID
import data.video.YoutubeBB
# import data.video.Dataset
from model.Localization import Localization, LocalizationLoss


def parse_arguments():
    parser = argparse.ArgumentParser(description='Localization training')
    parser.add_argument('--dest', type=str, dest='dest', required=True, help='training directory path')
    parser.add_argument('--backbone', type=str, dest='backbone', default=None, help='backbone directory path')
    parser.add_argument('--data', type=str, dest='data', required=True, help='path to training data')
    parser.add_argument('--workers', type=int, dest='workers', default=32, help='number of data loading workers')
    parser.add_argument('--epochs', type=int, dest='epochs', default=15, help='number of total epochs to run')
    parser.add_argument('--start-epoch', type=int, dest='start_epoch', default=0, help='manual epoch number')
    parser.add_argument('--batch-size', type=int, dest='batch_size', default=64, help='mini-batch size of all gpus')
    parser.add_argument('--start-lr', type=float, dest='start_lr', default=1e-1, help='inital learning rate')
    parser.add_argument('--end-lr', type=float, dest='end_lr', default=1e-8, help='final learning rate')
    parser.add_argument('--momentum', type=float, dest='momentum', default=0.9, help='momentum')
    parser.add_argument('--weight-decay', type=float, dest='weight_decay', default=5e-4, help='weight decay')
    parser.add_argument('--gpus', nargs='+', type=int, dest='gpus', default=list(range(torch.cuda.device_count())), help='list of gpus to use for training')
    parser.add_argument('--resume', action='store_true', dest='resume')
    return parser.parse_args()


class ImageDataset(torch.utils.data.Dataset):
    def __init__(self, dataset):
        self._dataset = dataset

    def __getitem__(self, index):
        image = self._dataset[index]
        while len(image.objects) == 0:
            image = random.choice(self._dataset)
        image.objects = [random.choice(image.objects)]

        if random.random() < 0.5:
            image = data.image.Transformation.Flip(dim=0)(image)

        if random.random() < 0.25:
            image = data.image.Transformation.Grayscale()(image)

        mean = tuple(map(int, image.image.mean(axis=(0, 1))))

        # Create exemplar patch
        center = (image.objects[0].box[0] + image.objects[0].box[1]) / 2.0
        w, h = (image.objects[0].box[1] - image.objects[0].box[0] + 1.0)
        p = (w + h) / 2.0
        A = math.sqrt((w+p)*(h+p))
        p1, p2 = center - (A - 1.0) / 2.0, center + (A - 1.0) / 2.0
        cropping = numpy.stack([p1, p2])

        # cropping = image[0].box
        # padding = numpy.tile(0.05 * (cropping[1] - cropping[0] + 1.0), (2, 1))  # 10% padding in exemplar image
        padding = numpy.zeros((2, 2), dtype=numpy.float32)
        exemplar = data.image.Transformation.CropAndPad(
            cropping=cropping,
            padding=padding,
            new_shape=(127, 127),
            interpolation=cv2.INTER_CUBIC,
            border=cv2.BORDER_CONSTANT,
            fill=mean
        )(image)
        exemplar_patch = numpy.transpose(a=exemplar.image, axes=(2, 0, 1))
        exemplar_patch = torch.as_tensor(exemplar_patch, dtype=torch.float) / 255.0

        # Create search patch
        A = 2*math.sqrt((w + p) * (h + p))
        p1, p2 = center - (A - 1.0) / 2.0, center + (A - 1.0) / 2.0
        cropping = numpy.stack([p1, p2])
        padding = numpy.zeros((2, 2), dtype=numpy.float32)

        scale = random.uniform(0.5, 2.0)
        ratio = random.uniform(3.0/4.0, 4.0/3.0)
        scale_x, scale_y = scale * ratio, scale / ratio
        # scale_x = 1.0 + random.uniform(-0.15, 0.15)
        # scale_y = 1.0 + random.uniform(-0.15, 0.15)
        search = data.image.Transformation.Warp(
            cropping=cropping,
            padding=padding,
            scale=(scale_x, scale_y),
            rotation=0.0,  # random.uniform(-1.5, 1.5),
            shear=0.0,
            translation=(random.uniform(-64.0, 64.0), random.uniform(-64.0, 64.0)),
            new_shape=(255, 255),
            interpolation=cv2.INTER_CUBIC,
            border=cv2.BORDER_CONSTANT,
            fill=mean
        )(image)

        # Apply augmentations
        search = data.image.Transformation.AdjustBrightness(
            brightness_factor=random.uniform(3.0/4.0, 4.0/3.0)
        )(search)
        search = data.image.Transformation.AdjustContrast(
            contrast_factor=random.uniform(3.0/4.0, 4.0/3.0)
        )(search)
        search = data.image.Transformation.AdjustSaturation(
            saturation_factor=random.uniform(3.0/4.0, 4.0/3.0)
        )(search)
        if random.random() < 0.2:
            search = data.image.Transformation.MotionBlur()(search)
        search = data.image.Transformation.SaltAndPepperNoise(
            threshold=random.uniform(0.0, 0.03)
        )(search)

        search_patch = numpy.transpose(a=search.image, axes=(2, 0, 1))
        search_patch = torch.as_tensor(search_patch, dtype=torch.float) / 255.0

        # Create target box
        target_box = numpy.roll(a=search.objects[0].box, shift=1, axis=1)
        # target_box = search[0].box
        target_box = numpy.concatenate([(target_box[0] + target_box[1]) / 2.0, (target_box[1] - target_box[0] + 1.0)])
        target_box = torch.as_tensor(target_box, dtype=torch.float)

        return search_patch, exemplar_patch, target_box

    def __len__(self):
        return len(self._dataset)


class VideoDataset(torch.utils.data.Dataset):
    def __init__(self, dataset, max_distance=3, epochs=1):
        self._dataset = dataset
        self._max_distance = max_distance
        self._epochs = epochs

    def __getitem__(self, index):
        index = index % len(self._dataset)
        video = self._dataset[index]
        while len(video.frames) <= 3:
            video = random.choice(self._dataset)
        offset = random.randint(0, len(video.frames) - self._max_distance - 1)
        [search_index, exemplar_index] = random.sample(range(offset, offset+self._max_distance+1), 2)

        flip = random.random() < 0.5
        grayscale = random.random() < 0.25

        # Create exemplar patch
        image = video.frames[exemplar_index]
        image = data.image.Transformation.Flip(dim=0)(image) if flip else image
        image = data.image.Transformation.Grayscale()(image) if grayscale else image
        mean = tuple(map(int, image.image.mean(axis=(0, 1))))
        center = (image.objects[0].box[0] + image.objects[0].box[1]) / 2.0
        w, h = (image.objects[0].box[1] - image.objects[0].box[0] + 1.0)
        p = (w + h) / 2.0
        A = math.sqrt((w + p) * (h + p))
        p1, p2 = center - (A - 1.0) / 2.0, center + (A - 1.0) / 2.0
        cropping = numpy.stack([p1, p2])
        padding = numpy.zeros((2, 2), dtype=numpy.float32)
        # exemplar = data.image.Transformation.CropAndPad(
        #     cropping=cropping,
        #     padding=padding,
        #     new_shape=(127, 127),
        #     interpolation=cv2.INTER_CUBIC,
        #     border=cv2.BORDER_CONSTANT,
        #     fill=mean
        # )(image)

        scale = random.uniform(0.9, 1.1)
        ratio = random.uniform(9.0 / 10.0, 10.0 / 9.0)
        scale_x, scale_y = scale * ratio, scale / ratio

        exemplar = data.image.Transformation.Warp(
            cropping=cropping,
            padding=padding,
            scale=(scale_x, scale_y),
            rotation=0.0,  # random.uniform(-1.5, 1.5),
            shear=0.0,
            translation=(random.uniform(-4.0, 4.0), random.uniform(-4.0, 4.0)),
            new_shape=(127, 127),
            interpolation=cv2.INTER_CUBIC,
            border=cv2.BORDER_CONSTANT,
            fill=mean
        )(image)



        exemplar_patch = numpy.transpose(a=exemplar.image, axes=(2, 0, 1))
        exemplar_patch = torch.as_tensor(exemplar_patch, dtype=torch.float) / 255.0

        # Create search patch
        image = video.frames[search_index]
        image = data.image.Transformation.Flip(dim=0)(image) if flip else image
        image = data.image.Transformation.Grayscale()(image) if grayscale else image
        mean = tuple(map(int, image.image.mean(axis=(0, 1))))
        center = (image.objects[0].box[0] + image.objects[0].box[1]) / 2.0
        w, h = (image.objects[0].box[1] - image.objects[0].box[0] + 1.0)
        p = (w + h) / 2.0
        A = 2 * math.sqrt((w + p) * (h + p))
        p1, p2 = center - (A - 1.0) / 2.0, center + (A - 1.0) / 2.0
        cropping = numpy.stack([p1, p2])
        padding = numpy.zeros((2, 2), dtype=numpy.float32)

        scale = random.uniform(0.5, 2.0)
        ratio = random.uniform(3.0 / 4.0, 4.0 / 3.0)
        scale_x, scale_y = scale * ratio, scale / ratio

        search = data.image.Transformation.Warp(
            cropping=cropping,
            padding=padding,
            scale=(scale_x, scale_y),
            rotation=0.0,  # random.uniform(-1.5, 1.5),
            shear=0.0,
            translation=(random.uniform(-64.0, 64.0), random.uniform(-64.0, 64.0)),
            new_shape=(255, 255),
            interpolation=cv2.INTER_CUBIC,
            border=cv2.BORDER_CONSTANT,
            fill=mean
        )(image)

        # Apply augmentations
        search = data.image.Transformation.AdjustBrightness(
            brightness_factor=random.uniform(5.0 / 6.0, 6.0 / 5.0)
        )(search)
        search = data.image.Transformation.AdjustContrast(
            contrast_factor=random.uniform(5.0 / 6.0, 6.0 / 5.0)
        )(search)
        search = data.image.Transformation.AdjustSaturation(
            saturation_factor=random.uniform(5.0 / 6.0, 6.0 / 5.0)
        )(search)
        search = data.image.Transformation.SaltAndPepperNoise(
            threshold=random.uniform(0.0, 0.02)
        )(search)

        search_patch = numpy.transpose(a=search.image, axes=(2, 0, 1))
        search_patch = torch.as_tensor(search_patch, dtype=torch.float) / 255.0

        # Create target box
        target_box = numpy.roll(a=search.objects[0].box, shift=1, axis=1)
        target_box = numpy.concatenate([(target_box[0] + target_box[1]) / 2.0, (target_box[1] - target_box[0] + 1.0)])
        target_box = torch.as_tensor(target_box, dtype=torch.float)

        return search_patch, exemplar_patch, target_box

    def __len__(self):
        return len(self._dataset) * self._epochs


class AverageMeter(object):
    def __init__(self, name):
        self.name = name
        self.reset()

    def reset(self):
        self.val = 0.0
        self.sum = 0.0
        self.count = 0.0

    def update(self, val, n):
        self.val = val
        self.sum += val * n
        self.count += n

    def __str__(self):
        return '{}[B:{:2.5f}, A:{:2.5f}]'.format(self.name, self.val, self.sum / self.count)


def train(data_loader, model, optimizer, epoch):
    # bg_score_loss_meter = AverageMeter('BGScoreLoss')
    score_loss_meter = AverageMeter('ScoreLoss')
    box_loss_meter = AverageMeter('BoxLoss')
    loss_meter = AverageMeter('Loss')

    model.train()

    model.module.localization.darknet.eval()
    for param in model.module.localization.darknet.parameters():
        param.requires_grad = False

    # model.module.localization.darknet.layer3.train()
    # for param in model.module.localization.darknet.layer4.parameters():
    #     param.requires_grad = True

    for i in range(3, 5):
        model.module.localization.darknet.layers[i].train()
        for param in model.module.localization.darknet.layers[i].parameters():
            param.requires_grad = True

    for step, (search_patch, exemplar_patch, target) in enumerate(data_loader):
        batch_size = search_patch.shape[0]
        search_patch = search_patch.cuda(non_blocking=True)
        exemplar_patch = exemplar_patch.cuda(non_blocking=True)
        target = target.cuda(non_blocking=True)

        # Compute training output
        bg_score_loss, fg_score_loss, fg_box_loss = model(search_patch, exemplar_patch, target)
        score_loss = (torch.sum(bg_score_loss, dim=0) + torch.sum(fg_score_loss, dim=0)) / (2.0 * batch_size)
        box_loss = torch.sum(fg_box_loss, dim=0) / batch_size
        loss = score_loss + 5.0 * box_loss

        # Perform gradient descent
        optimizer.zero_grad()
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), 10.0)
        optimizer.step()

        # Update loss and accuracy
        score_loss_meter.update(val=score_loss.item(), n=batch_size)
        # _score_loss_meter.update(val=fg_score_loss.item(), n=batch_size)
        box_loss_meter.update(val=box_loss.item(), n=batch_size)
        loss_meter.update(val=loss.item(), n=batch_size)
        logging.info('Train | Epoch[{:5d}, {:5d}] {!s} {!s} {!s}'.format(epoch, step, loss_meter, score_loss_meter, box_loss_meter))

        if step % 500 == 0 and step > 0:
            checkpoint = {
                'model': model.module.state_dict(),
                'optimizer': optimizer.state_dict(),
                'epoch': epoch
            }
            torch.save(checkpoint, os.path.join(args.dest, 'checkpoint.pt'))
            torch.save(model.module.localization.state_dict(), os.path.join(args.dest, 'model.pt'))


def validate(data_loader, model, epoch):
    score_loss_meter = AverageMeter('ScoreLoss')
    box_loss_meter = AverageMeter('BoxLoss')
    loss_meter = AverageMeter('Loss')

    model.eval()
    with torch.no_grad():
        for step, (search_patch, exemplar_patch, target) in enumerate(data_loader):
            batch_size = search_patch.shape[0]
            search_patch = search_patch.cuda(non_blocking=True)
            exemplar_patch = exemplar_patch.cuda(non_blocking=True)
            target = target.cuda(non_blocking=True)

            # Compute training output
            bg_score_loss, fg_score_loss, fg_box_loss = model(search_patch, exemplar_patch, target)
            score_loss = (torch.sum(bg_score_loss, dim=0) + torch.sum(fg_score_loss, dim=0)) / (2.0 * batch_size)
            box_loss = torch.sum(fg_box_loss, dim=0) / batch_size
            loss = score_loss + 5.0 * box_loss

            # Update loss and accuracy
            score_loss_meter.update(val=score_loss.item(), n=batch_size)
            # _score_loss_meter.update(val=fg_score_loss.item(), n=batch_size)
            box_loss_meter.update(val=box_loss.item(), n=batch_size)
            loss_meter.update(val=loss.item(), n=batch_size)
            logging.info('Val | Epoch[{:5d}, {:5d}] {!s} {!s} {!s}'.format(epoch, step, loss_meter, score_loss_meter,
                                                                             box_loss_meter))
    return loss_meter.sum / loss_meter.count


if __name__ == '__main__':
    args = parse_arguments()

    # Create model and optimizer
    torch.cuda.set_device(args.gpus[0])
    model = Localization(config=Config).cuda()
    model = LocalizationLoss(localization=model, config=Config)
    model = torch.nn.DataParallel(model, device_ids=args.gpus, dim=0, output_device=args.gpus[0]).cuda()
    optimizer = torch.optim.SGD(model.parameters(), lr=args.start_lr, momentum=args.momentum, weight_decay=args.weight_decay)
    # optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate, weight_decay=args.weight_decay)
    lr_scheduler = torch.optim.lr_scheduler.ExponentialLR(
        optimizer=optimizer,
        gamma=(args.end_lr / args.start_lr) ** (1.0 / (args.epochs - 1.0)),
    )

    # Resume from checkpoint or create new training directory
    if not os.path.exists(args.dest):
        os.makedirs(args.dest)
    logging.basicConfig(filename=os.path.join(args.dest, 'logging.log'), level=logging.DEBUG)
    logging.info(args)
    if args.backbone is not None:
        if os.path.exists(os.path.join(args.backbone, 'best_model.pt')):
            logging.info('Loading model "{}".'.format(os.path.join(args.backbone, 'best_model.pt')))
            checkpoint = torch.load(os.path.join(args.backbone, 'best_model.pt'))
            model.module.localization.darknet.load_state_dict(checkpoint, strict=True)
        else:
            logging.error('Model "{}" does not exist.'.format(os.path.join(args.backbone, 'best_model.pt')))
    if args.resume:
        if os.path.join(args.dest, 'checkpoint.pt'):
            logging.info('Loading checkpoint "{}".'.format(os.path.join(args.dest, 'checkpoint.pt')))
            checkpoint = torch.load(os.path.join(args.dest, 'checkpoint.pt'))
            model.module.load_state_dict(checkpoint['model'], strict=True)
            optimizer.load_state_dict(checkpoint['optimizer'])
            args.start_epoch = checkpoint['epoch']
        else:
            logging.error('Checkpoint "{}" does not exist.'.format(os.path.join(args.dest, 'checkpoint.pt')))

    torch.backends.cudnn.benchmark = True

    # Create data loaders
    train_data_loader = torch.utils.data.dataloader.DataLoader(
        dataset=torch.utils.data.ConcatDataset([
            ImageDataset(
                dataset=data.image.ILSVRC_LOC.Reader(
                    root_path=os.path.join(args.data, 'ILSVRC'),
                    split='train',
                    blacklist=True
                )
            ),
            ImageDataset(
                dataset=data.image.COCO.Reader(
                    root_path=os.path.join(args.data, 'COCO'),
                    split='train',
                    year=2017
                )
            ),
            VideoDataset(
                dataset=data.video.YoutubeBB.Reader(
                    root_path=os.path.join(args.data, 'YoutubeBB', 'train')
                ),
                max_distance=3,
                epochs=5
            )
        ]),
        batch_size=args.batch_size,
        shuffle=True,
        num_workers=args.workers,
        pin_memory=True,
        drop_last=True
    )

    # Run training loop
    # best_loss = float('inf')
    for epoch in range(args.start_epoch, args.epochs):
        train(
            data_loader=train_data_loader,
            model=model,
            optimizer=optimizer,
            epoch=epoch
        )
        # loss = validate(
        #     data_loader=val_data_loader,
        #     model=model,
        #     epoch=epoch
        # )
        checkpoint = {
            'model': model.module.state_dict(),
            'optimizer': optimizer.state_dict(),
            'epoch': epoch + 1
        }
        torch.save(checkpoint, os.path.join(args.dest, 'checkpoint.pt'))
        torch.save(model.module.localization.state_dict(), os.path.join(args.dest, 'model.pt'))
        # if loss < best_loss:
        #     torch.save(checkpoint, os.path.join(args.dest, 'best_checkpoint.pt'))
        #     torch.save(model.module.localization.state_dict(), os.path.join(args.dest, 'best_model.pt'))
        #     best_loss = loss
        lr_scheduler.step()
    logging.info('Training finished.')
