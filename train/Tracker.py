import random
import numpy
import cv2
import torch
import torchvision
import torch.utils.data
import logging
import model.Tracker
import os
import sys
import shutil
import argparse
import data.video.Container
import data.video.Dataset


torch.set_num_threads(100)
torch.backends.cudnn.benchmark = True


class ChooseTracklet(data.Video.Dataset.Dataset):
    def __init__(self, dataset, length):
        self.dataset = dataset
        self.length = length

    def __getitem__(self, index):
        video = self.dataset[index]
        id, (start, length) = self._choose_tracklet(video)
        if length > self.length:
            offset = numpy.random.randint(low=0, high=length - self.length + 1)
            start = start + offset
            length = self.length
        frames = [video[start+t] for t in range(length)]
        video = data.Video.Container.Video(name=video.name, frames=frames)
        return video

    @staticmethod
    def _choose_tracklet(video):
        tracklets = []
        last = {}
        for t, frame in enumerate(video):
            current = {}
            for object in frame:
                # Continue old or initialize new tracklets
                elem = last.pop(object.id, (t, 0))
                current[object.id] = (elem[0], elem[1]+1)
            # Finalize complete tracklets
            tracklets.extend(last.items())
            last = current
        tracklets.extend(last.items())

        # Choose one tracklet according to lengths
        p = numpy.array(object=[tracklet[1][1] for tracklet in tracklets], dtype=numpy.float32)
        p = p/numpy.sum(p)
        index = numpy.random.choice(a=len(tracklets), p=p)
        return tracklets[index]

    def __len__(self):
        return len(self.dataset)


class TDataset(data.Video.Dataset.Dataset):
    def __init__(self, dataset):
        self.dataset = dataset
        self.augmentations = data.Video.Transformation.RandomTimeLapse((1, 3))

    def __getitem__(self, index):
        video = self.dataset[index]
        # first = random.randint(0, len(video)-2)
        # second = random.randint(first+1, len(video)-1)
        # video.frames = [video[first], video[second]]
        video = self.augmentations(video)
        video.frames = video.frames[0:2]
        return video

    def __len__(self):
        return len(self.dataset)


class TrainDataset(data.Video.Dataset.Dataset):
    def __init__(self, dataset, epochs):
        self.dataset = dataset
        self.epochs = epochs
        self.augmentations = data.Transformation.RandomOrder([
            data.Video.Transformation.SaltAndPepperNoise(0.01),
            data.Video.Transformation.RandomAdjustBrightness((0.4, 1.8)),
            data.Video.Transformation.RandomAdjustSaturation((0.2, 1.8)),
            data.Video.Transformation.RandomAdjustHue((-0.1, 0.1)),
            data.Video.Transformation.RandomAdjustContrast((0.5, 1.5)),
            data.Video.Transformation.RandomFlip(1, 0.5),
            data.Video.Transformation.RandomFlip(2, 0.5),
            data.Video.Transformation.RandomAffine(
                angle=(-2.0, +2.0),
                translate=((-10.0, +10.0), (-10.0, +10.0)),
                scale=((0.8, 1.2), (0.8, 1.2)),
                shear=((0.0, 0.0), (0.0, 0.0)),
                interpolation=cv2.INTER_CUBIC,
                fillcolor=(0, 0, 0)
            )
        ])

    def __getitem__(self, index):
        index = index % len(self.dataset)

        video = self.dataset[index]
        video = self.augmentations(video)
        frames, boxes = [], []
        for frame in video:
            frames.append(torchvision.transforms.ToTensor()(frame.image))
            poly = frame[0].polygons[0]
            xmin, ymin = numpy.min(poly, 0)
            xmax, ymax = numpy.max(poly, 0)
            box = torch.tensor([
                (ymin + ymax) / 2.0,
                (xmin + xmax) / 2.0,
                ymax - ymin + 1.0,
                xmax - xmin + 1.0
            ])
            boxes.append(box)
        frames, boxes = torch.stack(frames), torch.stack(boxes)
        return frames, boxes

    def __len__(self):
        return len(self.dataset) * self.epochs


class TrainModel(torch.nn.Module):
    def __init__(self):
        super(TrainModel, self).__init__()
        self.tracker = model.Tracker.Tracker(model.Tracker.Config())

    def forward(self, frames, targets):
        return self.tracker.loss(frames, targets)

####################################################################################################
## Training
####################################################################################################

def train(dest, name, data_name, gpus, load):
    root_path = os.path.join(dest, name)
    data_path = os.path.join(dest, data_name)
    logger_path = os.path.join(root_path, 'logging.log')
    checkpoint_path = os.path.join(root_path, 'checkpoint.pt')

    if os.path.exists(root_path) and not load:
        shutil.rmtree(root_path)

    if not os.path.exists(root_path):
        os.makedirs(root_path)

    torch.cuda.set_device(gpus[0])
    tracker = torch.nn.DataParallel(TrainModel(), device_ids=gpus, dim=1).cuda()
    optimizer = torch.optim.Adam(tracker.parameters(), lr=0.001, weight_decay=0.005)
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer=optimizer, step_size=1, gamma=0.5)

    # if os.path.exists(checkpoint_path) and load:
    #     state_dict = torch.load(checkpoint_path)
    #     tracker.module.load_state_dict(state_dict['model'])
    #     optimizer.load_state_dict(state_dict['optimizer'])
    #     # epoch = state_dict['epoch']
    #     step = state_dict['step']

    logging.basicConfig(level=logging.DEBUG, handlers=[logging.FileHandler(filename=logger_path), logging.StreamHandler()])

    dataset = data.video.Dataset.Dataset(data_path)
    dataset = ChooseTracklet(dataset, 4)
    dataset = TDataset(dataset)
    dataset = TrainDataset(dataset, 10000)
    data_loader = torch.utils.data.dataloader.DataLoader(dataset=dataset, batch_size=80, shuffle=True, num_workers=20, pin_memory=True)
    tracker.train()
    running_loss = 0.0
    for epoch in range(16):
        lr_scheduler.step()
        for step, (frames, boxes) in enumerate(data_loader):
            frames, targets = frames.cuda(non_blocking=True).transpose(0, 1), boxes.cuda(non_blocking=True).transpose(0, 1)
            optimizer.zero_grad()
            loss = tracker(frames, targets)
            loss = torch.sum(loss)
            loss.backward()
            torch.nn.utils.clip_grad_norm_(tracker.parameters(), 1.0)
            optimizer.step()
            running_loss = 0.99*running_loss+0.01*loss.item()
            logging.info(name+' [%5d, %5d] moving_loss: %.10f, loss: %.10f' % (epoch, step, running_loss, loss.item()))
            del loss
            if step > 0 and step % 500 == 0:
                state_dict = {
                    'model': tracker.module.state_dict(),
                    'optimizer': optimizer.state_dict()
                }
                torch.save(state_dict, checkpoint_path)

            if step > 10000:
                break

        state_dict = {
            'model': tracker.module.tracker.state_dict(),
            'optimizer': optimizer.state_dict()
        }
        torch.save(state_dict, checkpoint_path)

    print("Training finished!")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Command line options')
    parser.add_argument('--dest', type=str, dest='dest', required=True, help='destination directory')
    parser.add_argument('--name', type=str, dest='name', required=True, help='name of training run')
    parser.add_argument('--data', type=str, dest='data', required=True, help='name of training data')
    parser.add_argument('--gpus', nargs='+', type=int, dest='gpus', default=list(range(torch.cuda.device_count())), help='list of gpus to use for training')
    parser.add_argument('--load', action='store_true', dest='load')
    args = parser.parse_args(sys.argv[1:])
    train(args.dest, args.name, args.data, args.gpus, args.load)
