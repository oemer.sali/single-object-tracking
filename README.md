# Single Object Tracking
For a given video sequence with a bounding box around a target object in the first frame, the task in Single Object Tracking is to localize the object in all subsequent frames. Below we present the results of our tracker on the 100 video sequences of the [Object Tracking Benchmark (OTB100)](https://faculty.ucmerced.edu/mhyang/papers/pami15_tracking_benchmark.pdf).

## Qualitative Results
The following videos are selected examples from the 100 sequences of the OTB dataset. The ground truth boxes are coloured green and the predicted output of our tracker is given in red. Maximize the videos to better see the boxes.

### Successful Examples
<table>
<tr>
<td>![ ](result/qualitative/00004.mp4)</td>
<td>![ ](result/qualitative/00007.mp4)</td>
</tr>
<tr>
<td>![ ](result/qualitative/00009.mp4)</td>
<td>![ ](result/qualitative/00027.mp4)</td>
</tr>
<tr>
<td>![ ](result/qualitative/00040.mp4)</td>
<td>![ ](result/qualitative/00044.mp4)</td>
</tr>
<tr>
<td>![ ](result/qualitative/00082.mp4)</td>
<td>![ ](result/qualitative/00070.mp4)</td>
</tr>
<tr>
<td>![ ](result/qualitative/00055.mp4)</td>
<td>![ ](result/qualitative/00072.mp4)</td>
</tr>
</table>

### Failure Cases
<table>
<tr>
<td>![Fast Motion / Motion Blur](result/qualitative/00010.mp4)</td>
<td>![Identity Switch](result/qualitative/00012.mp4)</td>
</tr>
<tr>
<td>![Large Object Deformation](result/qualitative/00091.mp4)</td>
<td>![Fast Scale Change](result/qualitative/00094.mp4)</td>
</tr>
</table>

## Quantitative Results
A commonly used evaluation metric is the success rate: For a given video sequence let $`G_i`$ and $`B_i`$ denote the ground truth and predicted bounding boxes of a target object in frame $`i`$, respectively. The overlap score of these bounding boxes is given by the intersection over union
```math
IoU_i=\frac{\lvert G_i\cap B_i\rvert}{\lvert G_i\cup B_i\rvert}.
```
For a fixed threshold $`\tau\in[0,1]`$, we call our tracker on the $`i`$-th frame successful, if $`IoU_i>\tau`$. The success rate of our tracker at threshold $`\tau`$ is the number of successful frames diveded by the total number of frames in the given video sequence. As the threshold varies between $`0`$ and $`1`$, the success rate changes and the resultant curve of our tracker (MemSiamRPN) is presented in the following plot together with the curves of various other trackers:

![ ](result/quantitative/success_plot_OPE_OTB100_AUC.png)

The numbers in the brackets denote the area under the curve (AUC) used to rank the trackers.
