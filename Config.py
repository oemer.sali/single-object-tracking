import math


# class ResNet(object):
#     # Shape (height, width) of input image
#     shape = (255, 255)
#     # Input channels
#     in_channels = 3
#     # Output channels
#     out_channels = 1024
#     # Structure of backbone network
#     structure = [
#         [(32, 2), (32, 1)],
#         [(64, 2), (64, 1)],
#         [(128, 2), (128, 1)],
#         [(256, 1), (256, 1), (256, 1)],
#         [(256, 1), (256, 1), (256, 1)],
#         [(256, 1), (256, 1), (256, 1)]
#     ]


class DarkNet(object):
    num_blocks = [1, 2, 8, 8, 4]

    num_channels = 32

    expansion = 2


class Backbone(object):
    # Shape (height, width) of input image
    shape = (255, 255)
    # Input channels
    in_channels = 3
    # Output channels
    out_channels = 1024
    # Structure of backbone network
    # structure = [
    #     [3, 1, 1], [3, 2, 0],
    #     [3, 1, 1], [3, 2, 0],
    #     [3, 1, 1], [1, 1, 0], [3, 1, 1], [3, 2, 0],
    #     [3, 1, 1], [1, 1, 0], [3, 1, 1], [3, 2, 0],
    #     [3, 1, 1], [1, 1, 0], [3, 1, 1], [1, 1, 0], [3, 1, 1], [3, 2, 0],
    #     [3, 1, 1], [1, 1, 0], [3, 1, 1], [1, 1, 0], [3, 1, 1]
    # ]
    structure = [
        [3, 1, 1],
        [3, 2, 0], [1, 1, 0], [3, 1, 1],
        [3, 2, 0], [1, 1, 0], [3, 1, 1],
        [3, 2, 0]
    ]


class BackboneLoss(object):
    num_classes = 1000

# class ResNetLoss(object):
#     num_classes = 1000


def _rec_field(shape, structure):
    def _rec_field_step(shape_in, stride_in, start_in, convolution):
        height_in, width_in = shape_in
        size, stride, padding = convolution

        height_out = math.floor((height_in - size + 2 * padding) / stride) + 1
        width_out = math.floor((width_in - size + 2 * padding) / stride) + 1
        stride_out = stride_in * stride
        start_out = start_in + ((size - 1.0) / 2.0 - padding) * stride_in

        return (height_out, width_out), stride_out, start_out

    stride, start = 1.0, 0.0
    for layer in structure:
        shape, stride, start = _rec_field_step(
            shape_in=shape,
            stride_in=stride,
            start_in=start,
            convolution=layer
        )
    return shape, stride, start


class RPN(object):
    shape, stride, start = _rec_field(Backbone.shape, Backbone.structure)

    scales = (64.0,)
    ratios = (1.0/3.0, 1.0/2.0, 1.0, 2.0, 3.0)

    threshold = 0.0

    window_influence = 0.5

    score_penalty = 0.23


class RPNLoss(object):
    thresholds = (0.3, 0.6)

    counts = (16, 48)


# class RPNHead(object):
#     input_channels = ResNet[-2]
#
#     adjust_channels = 256
#
#     num_anchors = len(RPN.scales) * len(RPN.ratios)


class SiameseRPNHead(object):
    input_channels = 1024

    adjust_channels = 128

    num_anchors = len(RPN.scales) * len(RPN.ratios)


class Tracker(object):
    exemplar_shape = (127, 127)

    search_shape = Backbone.shape

    hidden_size = 6

    observation_size = 4

    center_momentum = 1.0

    size_momentum = 0.25
