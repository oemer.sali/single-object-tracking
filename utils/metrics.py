import numpy as np
import torch


def iou(bboxA, bboxB):
    """Calculates the pairwise IOU of the given bounding boxes.

    :param bboxA: [time, batch, (x1, y1, x2, y2)]. Array of bounding boxes.
    :param bboxB: [time, batch, (x1, y1, x2, y2)]. Array of bounding boxes.

    :return iou: [time, batch]. Pairwise IOU of bounding boxes.
    """
    A1, A2 = torch.split(tensor=bboxA, split_size_or_sections=2, dim=2)
    B1, B2 = torch.split(tensor=bboxB, split_size_or_sections=2, dim=2)

    I1 = torch.max(A1, B1)
    I2 = torch.min(A2, B2)
    I = torch.prod(input=torch.clamp(input=I2-I1+1.0, min=0.0), dim=2, keepdim=False)

    U1 = torch.prod(input=A2-A1+1.0, dim=2, keepdim=False)
    U2 = torch.prod(input=B2-B1+1.0, dim=2, keepdim=False)
    U = U1 + U2 - I

    iou = torch.div(I, U)

    return iou


def precision(bboxA, bboxB):
    """Calculates the pairwise Precision of the given bounding boxes.

    :param bboxA: [time, batch, (x1, y1, x2, y2)]. Array of bounding boxes.
    :param bboxB: [time, batch, (x1, y1, x2, y2)]. Array of bounding boxes.

    :return precision: [time, batch]. Pairwise IOU of bounding boxes.
    """
    A1, A2 = torch.split(tensor=bboxA, split_size_or_sections=2, dim=2)
    B1, B2 = torch.split(tensor=bboxB, split_size_or_sections=2, dim=2)

    center1 = (A1 + A2) / 2.0
    center2 = (B1 + B2) / 2.0

    precision = torch.norm(input=center1-center2, p=2, dim=2, keepdim=False)

    return precision


def success(scores, start, end, step, greater):
    """Calculates the percentage of successful scores with respect to the given threshold.

    :param scores: [time, batch]. Array of scores.
    :param threshold: scalar. Threshold for succeeding scores.

    :return samples: [steps]. Sample points used to calculate the success rates.
    :return success: [batch, steps]. Success rates of the given scores.
    """
    comparator = torch.ge if greater else torch.le
    samples = torch.arange(start=start, end=end, step=step, dtype=torch.float32, device=scores.device)
    indicator = comparator(input=scores.unsqueeze(2), other=samples)

    count = torch.sum(input=indicator, dim=0, keepdim=False, dtype=torch.float32)
    success = torch.div(count, scores.shape[0])

    return samples, success


def auc(samples, success):
    """Calculates the area under the curve.

    :param samples: [steps]. Sample points used to calculate the success rates.
    :param success: [batch, steps]. Success rates at the given sample points.

    :return: [batch]. Area under the curve calculated for the piecewise linear function.
    """
    distances = samples[1:]-samples[:-1]
    partial_sums = success[:, :-1]+success[:, 1:]
    auc = torch.sum(input=distances * partial_sums, dim=1) / 2.0
    return auc

def param2bbox(param):
    """Transforms parameter coordinates to bounding box coordinates.

    :param param: [batch, 4]. Parameters in (center_y, center_x, height, width) coordinates.

    :return bbox: [batch, 4]. Bounding boxes in (x1, y1, x2, y2) coordinates.
    """
    center, size = torch.split(tensor=param, split_size_or_sections=2, dim=2)
    bbox = torch.cat(tensors=[center-(size-1.0)/2.0, center+(size-1.0)/2.0], dim=2)
    return bbox

def bbox2param(bbox):
    """Transforms bounding box coordinates to parameter coordinates.

    :param bbox: [batch, 4]. Bounding boxes in (x1, y1, x2, y2) coordinates.

    :return param: [batch, 4]. Parameters in (center_y, center_x, height, width) coordinates.
    """
    point1, point2 = torch.split(tensor=bbox, split_size_or_sections=2, dim=2)
    param = torch.cat(tensors=[(point1+point2)/2.0, point2-point1+1.0], dim=2)
    return param

# bboxA = torch.Tensor([[[0.0, 0.0, 10.0, 10.0], [5.0, 5.0, 15.0, 15.0]], [[0.0, 0.0, 10.0, 10.0], [5.0, 5.0, 15.0, 15.0]]])
# bboxB = torch.Tensor([[[1.0, 1.0, 10.0, 10.0], [1.0, 2.0, 20.0, 25.0]], [[1.0, 1.0, 10.0, 10.0], [1.0, 2.0, 20.0, 20.0]]])
#
# print(bboxA-param2bbox(bbox2param(bboxA)))
# I = iou(bboxA, bboxB)
# # P = precision(bboxA, bboxB)
# thresholds = torch.linspace(start=0.0, end=1.0, steps=10)
# SI = success(I, thresholds, True)
# # SP = success(P, 0.5)
#
# print("I", I)
# # print("P", P)
# print(thresholds)
# print("SI", SI)
# # print("SP", SP)
