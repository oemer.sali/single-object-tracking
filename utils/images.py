import torch


# def grid_sample(frame, bounding_box, target_shape):
#     """Crops and warps patches from frame according to bounding boxes with billiear interpolation.
#
#     :param frame: [batch, input_channels, input_height, input_width]. Input frames to crop.
#     :param bounding_box: [batch, 4]. Bounding boxes in (x1,y1,x2,y2) coordinates.
#     :param target_shape: [2]. Common (height, width) shape of cropped patch.
#
#     :return crop: [batch, input_channels]+target_shape. Croped and warped patches.
#     """
#     # Build target grid independent from bounding boxes
#     grid_x = torch.linspace(start=0.0, end=1.0, steps=target_shape[1], device=frame.device)
#     grid_x = torch.unsqueeze(input=grid_x, dim=0).repeat(target_shape[0], 1)
#     grid_y = torch.linspace(start=0.0, end=1.0, steps=target_shape[0], device=frame.device)
#     grid_y = torch.unsqueeze(input=grid_y, dim=1).repeat(1, target_shape[1])
#     grid = torch.stack(tensors=[grid_x, grid_y], dim=2) # grid_sample expects (x, y) coordinates
#     grid = torch.unsqueeze(input=grid, dim=0).repeat(frame.shape[0], 1, 1, 1)
#
#     # Shift and stretch the grid depending on bounding boxes
#     bounding_box = bounding_box.view(-1, 1, 1, 4)
#     pos1, pos2 = torch.split(tensor=bounding_box, split_size_or_sections=2, dim=3)
#     grid = (pos1+(pos2-pos1)*grid) # Shift grid between pos1 and pos2
#     full_size = torch.tensor(data=frame.shape[2:4], dtype=frame.dtype, device=frame.device) - 1.0
#     grid = 2.0 * grid / full_size - 1.0 # normalize grid to -1..1
#
#     # center = bounding_box[:, 0:2].view(-1, 1, 1, 2)
#     # size = bounding_box[:, 2:4].view(-1, 1, 1, 2) - 1.0
#     # full_size = torch.tensor(data=frame.shape[2:4], dtype=frame.dtype, device=frame.device) - 1.0
#     # grid = (center - size / 2.0 + grid * size) / full_size
#     # grid = 2.0 * grid - 1.0
#
#     # Sample given frames via grid
#     #grid = torch.flip(input=grid, dims=[3])
#     crop = torch.nn.functional.grid_sample(input=frame, grid=grid, mode='bilinear', padding_mode='zeros')
#     return crop

def conv_size(input_size, kernel_size, padding_size=0, stride_size=1):
    output_size = (input_size+2*padding_size-kernel_size)//stride_size+1
    return output_size


def grid_sample_p(frame, box, target_shape):
    """Crops and warps patches from frame according to bounding boxes with billiear interpolation.

    :param frame: [batch, input_channels, input_height, input_width]. Input frames to crop.
    :param box: [batch, 4]. Bounding boxes in (y, x, h, w) coordinates.
    :param target_shape: [2]. Common (height, width) shape of cropped patch.

    :return crop: [batch, input_channels]+target_shape. Croped and warped patches.
    """
    batch_size, channels, height, width = frame.shape
    cy, cx, sy, sx = torch.split(tensor=box, split_size_or_sections=1, dim=1)
    zeros = torch.zeros(batch_size, 1, dtype=box.dtype, device=box.device)
    theta = torch.cat(tensors=[
        sx / width, zeros, 2.0 * cx / (width - 1.0) - 1.0,
        zeros, sy / height, 2.0 * cy / (height - 1.0) - 1.0
    ], dim=1).view(-1, 2, 3)
    grid = torch.nn.functional.affine_grid(theta, torch.Size((batch_size, channels, target_shape[0], target_shape[1])))
    crop = torch.nn.functional.grid_sample(frame, grid)
    return crop


def grid_sample(frame, bounding_box, target_shape):
    """Crops and warps patches from frame according to bounding boxes with billiear interpolation.

    :param frame: [batch, input_channels, input_height, input_width]. Input frames to crop.
    :param bounding_box: [batch, 4]. Bounding boxes in (x1,y1,x2,y2) coordinates.
    :param target_shape: [2]. Common (height, width) shape of cropped patch.

    :return crop: [batch, input_channels]+target_shape. Croped and warped patches.
    """
    """
    [  x2-x1             x1 + x2 - W + 1  ]
    [  -----      0      ---------------  ]
    [  W - 1                  W - 1       ]
    [                                     ]
    [           y2-y1    y1 + y2 - H + 1  ]
    [    0      -----    ---------------  ]
    [           H - 1         H - 1       ]
    """
    batch_size, channels, height, width = frame.shape
    x1, y1, x2, y2 = torch.split(tensor=bounding_box, split_size_or_sections=1, dim=1)
    zeros = torch.zeros(batch_size, 1, dtype=bounding_box.dtype, device=bounding_box.device)
    theta = torch.cat(tensors=[
        (x2 - x1) / (width - 1.0), zeros, (x1 + x2 - width + 1.0) / (width - 1.0),
        zeros, (y2 - y1) / (height - 1.0), (y1 + y2 - height + 1.0) / (height - 1.0)
    ], dim=1).view(-1, 2, 3)
    grid = torch.nn.functional.affine_grid(theta, torch.Size((batch_size, channels, target_shape[0], target_shape[1])))
    crop = torch.nn.functional.grid_sample(frame, grid)
    return crop

def param2bbox(param):
    """Transforms parameter coordinates to bounding box coordinates.

    :param param: [batch, 4]. Parameters in (center_y, center_x, height, width) coordinates.

    :return bbox: [batch, 4]. Bounding boxes in (x1, y1, x2, y2) coordinates.
    """
    center, size = torch.split(tensor=param, split_size_or_sections=2, dim=1)
    bbox = torch.cat(tensors=[center-(size-1.0)/2.0, center+(size-1.0)/2.0], dim=1)
    return bbox

def bbox2param(bbox):
    """Transforms bounding box coordinates to parameter coordinates.

    :param bbox: [batch, 4]. Bounding boxes in (x1, y1, x2, y2) coordinates.

    :return param: [batch, 4]. Parameters in (center_y, center_x, height, width) coordinates.
    """
    point1, point2 = torch.split(tensor=bbox, split_size_or_sections=2, dim=1)
    param = torch.cat(tensors=[(point1+point2)/2.0, point2-point1+1.0], dim=1)
    return param


# t = torch.randn(1, 4).float()
# diff = t-bbox2param(param2bbox(t))
# # diff = t-param2bbox(bbox2param(t))
#
# print(diff)
# print(torch.mean(torch.abs(diff)))